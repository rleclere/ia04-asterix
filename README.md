# IA04 : Astérix et Obélix contre les romains

Ce projet a été réalisé par le groupe d'IA04 de P20 composé de : 
* Apolline Miannay
* Florentin Vanaldewereld
* Julien Scrève
* Valentin Rousseau
* Romain Leclere

## Bibliothèque utilisé 

Le programme utilise à la fois **Jade et Mason**. 

## Fonctionnement

Le projet peut se lancer directement. on observera quatre zones sur l'écran. À gauche correspond au camp romain, il y a cesar et des petits romains qui se balade( Piont noire) avant l'attaque. Au centre haut, on trouve le champ de bataille. En dessous, il y a la forêt dans laquelle pop des ressources pour les Gaulois : des sangliers et ingrédient de potion magique. Dans le camp tout à droite, on retrouve nos Gaulois préférer. Les points jaunes correspondent au Gaulois. Ceux-ci se baladent et reculte des sangliers quand Abraracoursix juge qu'il n'y en a plus assez dans les stocks. Astérix quant à lui prépare la prochaine attaque de Césars en récoltant les éléments pour la potion magique selon les besoins de Panoramix. Au cours du temps les Gaulois perdent de la santé, celle-ci est récupérée quand il mange des sangliers. Quand césars lancent l'assaut, les Romains se place sur le champ de bataille attendant l'ennemie gauloise, quant au Gaulois, il commence par boire de la potion magique avant d'aller combattre les Romaines. Une fois le combat finit nos chere gaulois fête leur victoire autour d'un bon banquet.

## Code couleur

Les romains sont des points noir quand ils sont full vie, entre 100% et 60%. Entre 60% et 30%, ils passent gris puis finisent blanc avant de mourrir, entre 30% et 0%.

Les gaulois sont des points jaunes quand ils sont full vie, entre 100% et 60%. Entre 60% et 30%, ils passent maron puis finisent rouge avant de mourrir, entre 30% et 0%. Si un gaulois prend de la potion magique il devient Vert.

Les petits points rose correspond au sanglier et les carré vert aux ingrédient de potion.

## Screen

Screen des moments de vie

![Screen des moments de vie](./res/life_time.png "Screen des moments de vie")

***

Screen de la course à la potion

![Screen de la course à la potion](./res/potion_time.png "Screen de la course à la potion")

***

Screen de la bataille

![Screen de la bataille](./res/battle_time.png "Screen de la bataille")