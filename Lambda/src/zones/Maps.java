package zones;

import java.util.Random;

import constants.Constants;
import ec.util.MersenneTwisterFast;
import lambda.Lambdas;
import ressources.Food;
import sim.util.Bag;
import sim.util.Int2D;

public class Maps {
	static public synchronized boolean supprimerSanglier(Int2D position) {
		Bag b = Lambdas.getInstance().getYard().getObjectsAtLocation(position);
		if (b != null) {
			Object[] agentsOnCell = b.objs;
			for (Object agent : agentsOnCell) {
				if (agent != null && agent.getClass() == Food.class) {
					try {
						Food food = (Food) agent;
						if (food.getResName().equals("Sanglier")) {
							Lambdas.getInstance().getYard().remove(agent);
							return true;
						}
					}
					catch(Exception nPE) {
						System.out.println("Erreur food");
					}
				}
			}
		}
		return false;
	}
	
	static public Int2D randomPosBetween(Int2D max, Int2D min) {
		MersenneTwisterFast rand = Lambdas.getInstance().random;
		int x = rand.nextInt(max.x - min.x) + min.x;
		int y = rand.nextInt(max.y - min.y) + min.y;
		return new Int2D(x, y);
	}
	
	static public boolean isBetween(Int2D pos, Int2D max, Int2D min) {
		return min.x <= pos.x && pos.x < max.x && min.y <= pos.y && pos.y < max.y;
	}
	
}
