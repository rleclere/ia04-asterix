package zones;
import java.util.ArrayList;
import java.util.List;

import lambda.RomainLambda;
import sim.util.Int2D;

public class champDeBataille extends zone {

	private static final long serialVersionUID = 1L;
	private static champDeBataille INSTANCE = null;
	public List<RomainLambda> romainsReady = new ArrayList<RomainLambda>();

	public champDeBataille() {
		
	}
	
	public static champDeBataille getInstance() {
		return INSTANCE;
	}
	@Override
	protected void setup() {
		// TODO Auto-generated method stub
		INSTANCE = this;
		addBehaviour(new waitingBehaviour(this)); // On lance le behaviour de l'agent
	}
	
	public List<RomainLambda> getRomainsReady() {
		return romainsReady;
	}

	public void setRomainsReady(List<RomainLambda> romainsReady) {
		this.romainsReady = romainsReady;
	}

}
