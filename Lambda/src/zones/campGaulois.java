package zones;

import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import constants.Constants;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import lambda.GauloisLambda;
import lambda.Lambda;
import lambda.Lambdas;
import personnagesSpeciaux.Gaulois.Abraracourcix;
import ressources.Food;
import ressources.Ressource;
import sim.util.Int2D;

public class campGaulois extends zone {

	private static final long serialVersionUID = 1L;
	private static campGaulois INSTANCE;
	private int nbGauloisLambdaRestant = Constants.N_GAULOIS_LAMBDA;

	@Override
	protected void setup() {
		// TODO Auto-generated method stub
		INSTANCE = this;
		addBehaviour(new waitingBehaviour(this)); // On lance le behaviour de l'agent
		addBehaviour(new waitingGaulois()); // On lance le behaviour de l'agent
	}

	public campGaulois() {
		// TODO Auto-generated constructor stub
	}
	public static campGaulois getInstance() {
		return INSTANCE;
	}
	
	private class waitingGaulois extends CyclicBehaviour {

		private static final long serialVersionUID = 1L;

		public waitingGaulois() {
		}

		@Override
		public void action() {
			ACLMessage message; // On attend un message
			// On n'autorise PAS le traitement d'inscription ou desinscription � ce niveau
			MessageTemplate NotInscription = MessageTemplate
					.not(MessageTemplate.MatchPerformative(Constants.inscriptionZone));
			MessageTemplate NotDesinscription = MessageTemplate
					.not(MessageTemplate.MatchPerformative(Constants.desinscriptionZone));
			MessageTemplate mt = MessageTemplate.and(NotInscription, NotDesinscription); // TODO Check si 軋 fonctionne
			message = receive(mt);
			if (message != null) {
				// TODO en fonction du message re輹
				switch (message.getPerformative()) {
				case ACLMessage.PROPOSE:
					switch (message.getSender().getLocalName()) {
					case "Panoramix":
						stockerPotion(); // Ajout de l'駲uivalent d'un chaudron de potion dans le stock
						break;
					default: // Gaulois Lambda
						stockerSanglier(message.getContent()); // Ajout d'un sanglier � la cuisine
						break;
					}
					break;
				case Constants.GLDemandeFood: // Envoi de sanglier s'il y en a assez
					addBehaviour(new send1Ressource("sanglier", message));
					break;
				case Constants.GLDemandePotion: // Envoi de potion s'il y en a assez
					addBehaviour(new send1Ressource("potion", message));
					break;
				case Constants.ObelixDemandeMenhir: // Envoi de menhir s'il y en a assez
					addBehaviour(new send1Ressource("menhir", message));
					break;
				case ACLMessage.QUERY_REF:
					switch (message.getSender().getLocalName()) {
					case "Obelix":
						stockerMenhir(); // Ajout d'un menhir au camp
						break;
					case "Asterix":
						addBehaviour(new remplirGourdeAsterix(message));
						break;
					}
					break;
				case ACLMessage.REQUEST_WHENEVER:
					switch (message.getSender().getLocalName()) {
					case "Panoramix": // Demande de v駻if du stock de potion
						addBehaviour(new sendRecapStockRessource("potion", message));
						break;
					case "Abraracourcix": // Demande de v駻if des stocks de sanglier
						addBehaviour(new sendRecapStockRessource("sanglier", message));
						break;
					case "Obelix": // Demande de v駻if des stocks de sanglier
						addBehaviour(new sendRecapStockRessource("menhir", message));
						break;
					}
					break;
				}
			} else {
				block();
			}
		}
	}

	// OBELIX
	public void stockerMenhir() {
		resGenerique.put("menhir", resGenerique.get("menhir") + Constants.N_MENHIRS_PAR_PROD);
	}
	// FIN OBELIX

	// PANORAMIX
	public void stockerPotion() {
		resGenerique.put("potion", resGenerique.get("potion") + Constants.N_PORTION_POTION_PAR_CHAUDRON);
	}

	// FIN PANORAMIX

	// Gaulois LAMBDA
	public void stockerSanglier(String sPos) {
		resGenerique.put("sanglier", resGenerique.get("sanglier") + 1);
		Pattern r = Pattern.compile("(\\d+).\\ (\\d+)");
		Matcher m = r.matcher(sPos);
		m.find();
		Int2D pos = new Int2D(Integer.parseInt(m.group(1)),Integer.parseInt(m.group(2)));
		
		Food sanglier = new Food(pos, "Sanglier", null);
		ArrayList<Ressource> tmp;
		if(this.resUnique.containsKey(pos)) {
			tmp = this.resUnique.get(pos);
		} else {
			tmp = new ArrayList<Ressource>(); 
			this.resUnique.put(pos, tmp);
		}
		tmp.add(sanglier);
	}
	
	
	public int getNbGauloisLambdaRestant() {
		return nbGauloisLambdaRestant;
	}

	public void setNbGauloisLambdaRestant(int nbGauloisLambdaRestant) {
		this.nbGauloisLambdaRestant = nbGauloisLambdaRestant;
	}

	protected class send1Ressource extends OneShotBehaviour { // Envoi une ressource � si on peut l'envoyer
		// dans un camps
		private static final long serialVersionUID = 1L;
		ACLMessage reply;
		int ressource = 0;
		String typeRessource;

		public send1Ressource(String typeRessource, ACLMessage message) {
			this.typeRessource = typeRessource;
			if (resGenerique.get(typeRessource) == null) {
				resGenerique.put(typeRessource, 0);
			} else if (resGenerique.get(typeRessource) > 0) {
				ressource = 1;
			}
			reply = message.createReply();
		}

		public synchronized void action() {
			reply.setContent(ressource+ "");
			if (ressource == 1) {
				resGenerique.put(typeRessource, resGenerique.get(typeRessource) - 1);
				if (typeRessource.equals("sanglier")) {
					Random r = new Random();
					ArrayList<Int2D> keys = new ArrayList<Int2D>(resUnique.keySet());
					if(keys.size() > 0) {
						try {
							Int2D posSang = (Int2D)keys.get(r.nextInt(keys.size()));
							if(resUnique.get(posSang).size() == 1) {
								resUnique.remove(posSang);
							} else {
								resUnique.get(posSang).remove(0);
							}
							Maps.supprimerSanglier(posSang);
						}
						catch(Exception e) {
							System.out.println("erreur");
						}
					}
				}
			}
			send(reply);
		}
	}

	protected class sendRecapStockRessource extends OneShotBehaviour { // Utiliser � la cr饌tion d'un personnage pour
																	// l'ajouter
		// dans un camps
		private static final long serialVersionUID = 1L;
		ACLMessage reply;
		int ressources = 0;

		public sendRecapStockRessource(String typeRessource, ACLMessage message) {
			if (resGenerique.get(typeRessource) == null) {
				resGenerique.put(typeRessource, 0);
			} else {
				ressources = resGenerique.get(typeRessource);
			}
			reply = message.createReply();
		}

		public void action() {
			reply.setContent(ressources + "");
			send(reply);
		}
	}

	protected class remplirGourdeAsterix extends OneShotBehaviour { // Envoi autant de potion que possible � Asterix
		// dans un camps
		private static final long serialVersionUID = 1L;
		ACLMessage message;
		int potions = 0;

		public remplirGourdeAsterix(ACLMessage message) {
			this.message = message;
			if (resGenerique.get("potion") == null) {
				resGenerique.put("potion", 0);
			} else if (resGenerique.get("potion") > 0) {
				potions = resGenerique.get("potion");
			}
		}

		public void action() {
			ACLMessage reply = message.createReply();
			int potionPourAsterix = Math.min(Integer.parseInt(message.getContent()), potions); // Minimum entre ce qui
																								// est demand� et ce
																								// qu'on a
			reply.setContent(potionPourAsterix + "");
			resGenerique.put("potion", resGenerique.get("potion") - potionPourAsterix);
			send(reply);
		}
	}
}
