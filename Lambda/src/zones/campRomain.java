package zones;

public class campRomain extends zone {

	private static final long serialVersionUID = 1L;
	private static campRomain INSTANCE = null;
	
	public campRomain() {
		
	}
	
	public static campRomain getInstance() {
		return INSTANCE;
	}
	
	@Override
	protected void setup() {
		// TODO Auto-generated method stub
		INSTANCE = this;
		addBehaviour(new waitingBehaviour(this)); // On lance le behaviour de l'agent
	}

}
