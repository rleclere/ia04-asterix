package zones;

import java.util.List;
import java.util.Map;

import constants.Constants;

import java.util.ArrayList;
import java.util.HashMap;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import ressources.Ressource;
import sim.util.Int2D;

public abstract class zone extends Agent {

	private static final long serialVersionUID = 1L;
	int nbRomainsLambda = 0;
	int nbGauloisLambda = 0;

	boolean AsterixHere = false;
	boolean ObelixHere = false;

	List<AID> agentsPresent = new ArrayList<AID>(); // Liste des agents pr�sent dans la zone
	Map<String, Integer> resGenerique = new HashMap<String, Integer>(); // Style Sangliers etc.
	Map<Int2D, ArrayList<Ressource>> resUnique = new HashMap<Int2D, ArrayList<Ressource>>(); // Ressource unique, on
																								// stock directement
																								// l'objet

	protected abstract void setup();

	protected class waitingBehaviour extends CyclicBehaviour {

		private static final long serialVersionUID = 1L;

		public waitingBehaviour(zone z) {
			super(z);
		}

		@Override
		public void action() {
			ACLMessage message; // On attend un message
			// On autorise que le traitement d'inscription ou desinscription � ce niveau
			MessageTemplate mt = MessageTemplate.or(MessageTemplate.MatchPerformative(Constants.inscriptionZone),
					MessageTemplate.MatchPerformative(Constants.desinscriptionZone));
			message = receive(mt);
			if (message != null) {
				// TODO en fonction du message re輹
				switch (message.getPerformative()) {
				case Constants.inscriptionZone:
					inscriptionAgent(message.getSender()); // Inscription
					break;
				case Constants.desinscriptionZone:
					desinscriptionAgent(message.getSender()); // Desinscription
				}
			} else {
				block();
			}
		}
	}

	public boolean inscriptionAgent(AID Agent) {
		if (agentsPresent.contains(Agent)) { // V駻ifier la syntaxe du contains � l'execution pour 黎re s皞 que 軋
												// marche comme il faut
			return false; // L'agent est d駛� pr駸ent dans la zone
		}
		agentsPresent.add(Agent); // On ajoute l'agent sinon
		if (Agent.getLocalName().contains("RomainLambda")) {
			nbRomainsLambda++;
		}
		if (Agent.getLocalName().contains("GauloisLambda")) {
			nbGauloisLambda++;
		}
		if (Agent.getLocalName().contains(Constants.asterix.getLocalName())) {
			this.setAsterixHere(true);
		}
		if (Agent.getLocalName().contains(Constants.obelix.getLocalName())) {
			this.setObelixHere(true);
		}
		return true; // On notifie que l'inscription s'est d駻oul� correctement
	}

	public boolean desinscriptionAgent(AID Agent) {
		if (!agentsPresent.contains(Agent)) { // V駻ifier la syntaxe du contains � l'execution pour 黎re s皞 que 軋
												// marche comme il faut
			return false; // L'agent n'est pas pr駸ent dans la zone
		}
		agentsPresent.remove(Agent); // V駻ifier le fonctionnement � l'execution
		if (Agent.getLocalName().contains("RomainLambda")) {
			nbRomainsLambda--;
		}
		if (Agent.getLocalName().contains("GauloisLambda")) {
			nbGauloisLambda--;
		}
		if (Agent.getLocalName().contains(Constants.asterix.getLocalName())) {
			this.setAsterixHere(false);
		}
		if (Agent.getLocalName().contains(Constants.obelix.getLocalName())) {
			this.setObelixHere(false);
		}
		return true; // On notifie que l'inscription s'est d駻oul� correctement
	}

	public List<AID> getAgentsPresent() {
		return agentsPresent;
	}

	public void setAgentsPresent(List<AID> agentsPresent) {
		this.agentsPresent = agentsPresent;
	}

	public boolean isAsterixHere() {
		return AsterixHere;
	}

	public void setAsterixHere(boolean asterixHere) {
		AsterixHere = asterixHere;
	}

	public boolean isObelixHere() {
		return ObelixHere;
	}

	public void setObelixHere(boolean obelixHere) {
		ObelixHere = obelixHere;
	}

	public int getNbGauloisLambda() {
		return nbGauloisLambda;
	}

	public void setNbGauloisLambda(int nbGauloisLambda) {
		this.nbGauloisLambda = nbGauloisLambda;
	}

	public void incNbGauloisLambda() {
		this.nbGauloisLambda++;
	}

	public int getNbRomainsLambda() {
		return nbRomainsLambda;
	}

	public void setNbRomainsLambda(int nbRomainsLambda) {
		this.nbRomainsLambda = nbRomainsLambda;
	}

	public boolean disponibiliteGenerique(String ressource, int quantiteVoulu) {
		if (quantiteVoulu <= resGenerique.get(ressource)) {
			return true;
		}
		return false;
	}

	public boolean disponibiliteUnique(String ressource) {
		if (resGenerique.get(ressource) == null) {
			return false;
		}
		return true;
	}
}
