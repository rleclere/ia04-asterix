package constants;

import jade.core.AID;
import jade.lang.acl.ACLMessage;
import sim.util.Int2D;

public class Constants {
	
	public static int N_ADVERSAIRES_ASTERIX = 2;
	public static int N_ADVERSAIRES_OBELIX = 5;

	public static int N_GAULOIS_LAMBDA = 20;
	public static int N_ROMAINS_LAMBDA = N_GAULOIS_LAMBDA + N_ADVERSAIRES_ASTERIX + N_ADVERSAIRES_OBELIX;
	
	public static int VIE = 200;
	public static int VIE_ROMAIN = (int)(VIE * 0.8);
	public static int VIE_GAULOIS = VIE;
	public static int FORCE = 100;
	public static int MORAL = 100;
	public static int ATTAQUE = 1;
	public static int INVENTAIRE = 5;
	public static int DISTANCE_DEPLACEMENT = 5;

	// ----- Valeur maximum avec boost ----- //
	// Peut �ｿｽtre diff�ｿｽrent pour les agents spéciaux ?
	public static int MAX_VIE_LAMBDA = 100;
	public static int MAX_FORCE_LAMBDA = 100;
	public static int MAX_MORAL_LAMBDA = 100;
	public static int MAX_ATTAQUE_LAMBDA = 100;
	
	
	// ----- Reprise d'energie etc. ----- //
	public static int SANGLIER = 5; // 5pv en plus si on mange un sanglier
	public static int SOUPE = 5;
	public static int DORMIR = 5;
	// Ajout d'autres element pour up le moral etc. ?
	
	public static double BOOST_POTION = 10; // Coefficient de multiplication des attributs du gaulois qui a pris de la potion
	public static int BOOST_POTION_ASTERIX = 2;
	public static int BOOST_POTION_OBELIX = 5;
	
	public static int TEMPS_POTION = 250; // Nombre de step correspondant � la dur�e d'action de la potion
	public static int PERIODE_CHECK_POTION_CAMP = 300;
	public static int TEMPS_PREPARATION_POTION = 125; // Temps n�cessaire � la production de la potion
	public static int N_PORTION_POTION_PAR_CHAUDRON = 50; // Quantit� de potion par production
	public static int TAILLE_GOURDE_ASTERIX = 2; // Quantit� de potion
	public static int MIN_POTION_DANS_CAMP = 40; // Min > Max pour qu'Ast�rix aille chercher plusieurs fois les ingr�dients potions

	
	public static int TEMPS_PREPARATION_MENHIR = 75; // Temps n�ｿｽcessaire �ｿｽ la production de la potion
	public static int PERIODE_CHECK_MENHIR_CAMP = 250;
	public static int N_MENHIRS_PAR_PROD = 5;
	public static int MIN_MEHNIRS_DANS_CAMP = 61;
	
	public static int PERIODE_CHECK_SANGLIER_CAMP = 10;
	public static int CAPACITE_SANGLIER_CAMP = 80;
	


	
	public static int N_ACTION_PERTE_VIE = 3;
	
	public static int STEP_AVANT_BATAILLE = 500;

	// ----- Les diff�ｿｽrents enums pour les ressources ----- //
	public static enum RessourceType {
		Food,
		Ingredient,
		Material;
	}

	public static enum LocationType {
		Forest,
		TradePort;
	}

	// ----- Les diff�ｿｽrentes valeurs pour les ressources ----- //
	public static int MAX_QUANTITY_FOOD = 15;
	public static int MAX_QUANTITY_INGREDIENT= 7;
	public static int MAX_QUANTITY_MATERIAL = 10;

	public static int MAX_WEIGHT_FOOD = 5;
	public static int MAX_WEIGHT_INGREDIENT= 5;
	public static int MAX_WEIGHT_MATERIAL = 5;
	
	public static int N_INGREDIENTS_POUR_CHAUDRON = MAX_QUANTITY_INGREDIENT;
	public static int MAX_BALADE = 5;
	// ----- Les diff�ｿｽrentes valeurs pour la reprﾃｩsentation ----- //
	public static int GRID_SIZE = 60;			//largeur de la grille
	
	public static int FORET_XMIN = GRID_SIZE / 20;		
	public static int FORET_XMAX = 2* GRID_SIZE / 3;			
	public static int FORET_YMIN = 3* GRID_SIZE / 4;		
	public static int FORET_YMAX = GRID_SIZE;		
	
	public static int CAMPG_XMIN = 2* GRID_SIZE / 3;		
	public static int CAMPG_XMAX = GRID_SIZE;		
	public static int CAMPG_YMIN = 0;			
	public static int CAMPG_YMAX = GRID_SIZE;			
	
	public static int CAMPR_XMIN = 0;		
	public static int CAMPR_XMAX = GRID_SIZE /20;		
	public static int CAMPR_YMIN = 0;			
	public static int CAMPR_YMAX = GRID_SIZE;	
	
	public static AID campGaulois = new AID("campGaulois", AID.ISLOCALNAME);
	public static AID campRomain = new AID("campRomain", AID.ISLOCALNAME);
	public static AID foret = new AID("foret", AID.ISLOCALNAME);
	public static AID champDeBataille = new AID("champDeBataille", AID.ISLOCALNAME);
	public static AID asterix = new AID("Asterix", AID.ISLOCALNAME);
	public static AID obelix = new AID("Obelix", AID.ISLOCALNAME);
	public static AID panoramix = new AID("Panoramix", AID.ISLOCALNAME);
	public static AID abraracourcix = new AID("Abraracourcix", AID.ISLOCALNAME);
	
	
	public static AID cesar = new AID("Cesar", AID.ISLOCALNAME);
	
	// Gaulois Lambda vers camp
	public final static int GLDemandeFood = ACLMessage.REQUEST;
	public final static int GLDonneSanglier = ACLMessage.PROPOSE;
	public final static int GLDemandePotion = ACLMessage.QUERY_IF;
	
	// Gaulois Lambda vers Abraracourcix
	public final static int GLDemandeDeTache = ACLMessage.REQUEST;
	public final static int GLTacheTermine = ACLMessage.REQUEST_WHEN;
	
	// Camp Gaulois vers Abraracourcix
	public final static int CGStock = ACLMessage.INFORM;
	
	// Obelix vers camp
	public final static int ObelixDonneMenhir = ACLMessage.QUERY_REF;
	public final static int ObelixDemandeMenhir = ACLMessage.REQUEST_WHEN;
	public final static int ObelixCheckMenhir = ACLMessage.REQUEST_WHENEVER;
	
	// Pano vers camp
	public final static int PanoDonnePotion = ACLMessage.PROPOSE;
	public final static int PanoCheckStock = ACLMessage.REQUEST_WHENEVER;
	
	// Pano vers Asterix
	public final static int PanoDemandeIngredientsAsterix = ACLMessage.REQUEST;
	
	// Asterix vers camp
	public final static int AsterixDemandeRemplirGourde = ACLMessage.QUERY_REF;
	
	// Asterix vers Pano
	public final static int AsterixDonneIngredientsPano = ACLMessage.PROPOSE;
	
	// Abraracourcix vers camp
	public final static int AbraCheckStock = ACLMessage.REQUEST_WHENEVER;
	
	// Abraracourcix ou Cesar vers Gaulois Lambda ou Romain Lambda
	public final static int OrdreBataille = ACLMessage.PROPAGATE;
	public final static int demandeDeSuppression = ACLMessage.FAILURE;
	// AssuranceTourix vers Gaulois Lambda
	public final static int Laaaaaaa = ACLMessage.NOT_UNDERSTOOD;
	// D�ｿｽplacement entre les zones 
	public final static int inscriptionZone = ACLMessage.SUBSCRIBE;
	public final static int desinscriptionZone = ACLMessage.CANCEL;

	public static Int2D POSABRARACOURCIX = new Int2D(18 * GRID_SIZE / 20, 18 * GRID_SIZE / 20);
	public static int DELTAABRARACOURCIX = 3;
	public static Int2D POSPANORAMIX = new Int2D(18 * GRID_SIZE / 20, 2 * GRID_SIZE / 20);
	public static int DELTAPANORAMIX = 3;
	public static Int2D POSCESAR = new Int2D(GRID_SIZE / 40, GRID_SIZE / 2);
	public static Int2D MINPOSBANQUET = new Int2D(16 * GRID_SIZE / 20, 9 * GRID_SIZE / 20);
	public static Int2D MAXPOSBANQUET = new Int2D(19 * GRID_SIZE / 20, 12 * GRID_SIZE / 20);
	public static Int2D posMenhir = new Int2D(17 * GRID_SIZE / 20, 6 * GRID_SIZE / 20);
	public static int deltaMenhir = 3;
	
	public static int DEBUT_GUERRE = 0;
	public static int ALLER_BOIRE_POTION = 1;
	public static int INIT_POSITION_SUR_CHAMP = 2;
	public static int SE_BATTRE = 3;
	public static int FIN_GUERRE = 4;
	public static int BANQUET = 5;
	public static int ATTENDRE_GAULOIS = 6;
	public static int NOUVELLE_GARNISON = 7;
	
	public static String imageCesar = "./rsc/cesar.png";
	public static String imageAsterix = "./rsc/asterix.png";
	public static String imageObelix = "./rsc/obelix.png";
	public static String imagePanoramix = "./rsc/panoramix.png" ;
	public static String imageAbraracourcix = "./rsc/abraracourcix.png";
	public static String imageMenhir = "./rsc/menhir.png";
}
