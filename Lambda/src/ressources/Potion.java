package ressources;

import constants.Constants;
import sim.util.Int2D;

public class Potion extends Consumable {
    public Potion(Int2D positionInit, String resName, Constants.LocationType locationType) {
        super(positionInit, resName, locationType);
    }
}