package ressources;

import constants.Constants;
import sim.util.Int2D;

public class Material extends Ressource{
    public Material(Int2D positionInit, String resName, Constants.LocationType locationType) {
        super(positionInit);
        this.setResName(resName);
        this.setLocationType(locationType);
        // ... init (quantity, weight) ... //
        this.setQuantity(Constants.MAX_QUANTITY_MATERIAL);
        this.setWeight(Constants.MAX_WEIGHT_MATERIAL);

    }
}
