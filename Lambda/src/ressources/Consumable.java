package ressources;

import constants.Constants;
import sim.util.Int2D;

public abstract class Consumable extends Ressource{
    private int modHealth;
    private int modStrength;
    private int modSpeed;
    private int modReach;

    public Consumable(Int2D positionInit, String resName, Constants.LocationType locationType) {
        super(positionInit);
        this.setResName(resName);
        this.setLocationType(locationType);
    }


    // ----------BEGIN -- GETTER & SETTER -- BEGIN----------//

    public int getModHealth() {
        return modHealth;
    }

    public void setModHealth(int modHealth) {
        this.modHealth = modHealth;
    }

    public int getModStrength() {
        return modStrength;
    }

    public void setModStrength(int modStrength) {
        this.modStrength = modStrength;
    }

    public int getModSpeed() {
        return modSpeed;
    }

    public void setModSpeed(int modSpeed) {
        this.modSpeed = modSpeed;
    }

    public int getModReach() {
        return modReach;
    }

    public void setModReach(int modReach) {
        this.modReach = modReach;
    }

    // ------------END -- GETTER & SETTER -- END------------//
}
