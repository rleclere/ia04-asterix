package ressources;


import constants.Constants;
import sim.util.Int2D;

public class Food extends Consumable {
    public Food(Int2D positionInit, String resName, Constants.LocationType locationType) {
        super(positionInit, resName, locationType);
        // ... init (quantity, weight) ... //
        this.setQuantity(Constants.MAX_QUANTITY_FOOD);
        this.setWeight(Constants.MAX_WEIGHT_FOOD);
    }
}