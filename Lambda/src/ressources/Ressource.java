package ressources;

import constants.Constants;
import jade.core.Agent;
import sim.engine.SimState;
import sim.engine.Steppable;
import sim.engine.Stoppable;
import sim.util.Int2D;

public abstract class Ressource extends Agent implements Steppable {
    private Stoppable stoppable;
    private Int2D position;
    private int quantity;
    private String resName;
    private int weight;
    private Constants.LocationType locationType;

    public Ressource(Int2D positionInit) {
        this.setPosition(positionInit);
    }

    public int collectRes(int cap) {
        int collected = Math.min(this.quantity, cap);
        this.quantity -= collected;
        return collected;
    }

    private boolean isEmpty() {
        return this.quantity == 0;
    }

    private void remove() {
    }

    @Override
    public void step(SimState simState) {
    }

    // ----------BEGIN -- GETTER & SETTER -- BEGIN----------//

    public Stoppable getStoppable() {
        return stoppable;
    }

    public void setStoppable(Stoppable stoppable) {
        this.stoppable = stoppable;
    }

    public Int2D getPosition() {
        return position;
    }

    public void setPosition(Int2D position) {
        this.position = position;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getResName() {
        return resName;
    }

    public void setResName(String resName) {
        this.resName = resName;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Constants.LocationType getLocationType() {
        return locationType;
    }

    public void setLocationType(Constants.LocationType locationType) {
        this.locationType = locationType;
    }

    // ------------END -- GETTER & SETTER -- END------------//

}
