package ressources;

import constants.Constants;
import sim.util.Int2D;

public class Ingredient extends Ressource {
    public Ingredient(Int2D positionInit, String resName, Constants.LocationType locationType) {
        super(positionInit);
        this.setResName(resName);
        this.setLocationType(locationType);
        // ... init (quantity, weight) ... //
        this.setQuantity(Constants.MAX_QUANTITY_INGREDIENT);
        this.setWeight(Constants.MAX_WEIGHT_INGREDIENT);
    }
}
