package view;

import lambda.GauloisLambda;
import lambda.Lambdas;
import lambda.RomainLambda;
import personnagesSpeciaux.Gaulois.*;
import personnagesSpeciaux.Romain.Cesar;
import ressources.Food;
import ressources.Ingredient;
import sim.display.Controller;
import sim.display.Display2D;
import sim.display.GUIState;
import sim.engine.SimState;
import sim.portrayal.Portrayal;
import sim.portrayal.grid.SparseGridPortrayal2D;
import sim.portrayal.simple.ImagePortrayal2D;
import sim.portrayal.simple.OvalPortrayal2D;
import sim.portrayal.simple.RectanglePortrayal2D;
import zones.*;

import javax.sound.sampled.Port;
import javax.swing.*;

import constants.Constants;

import java.awt.*;

//Import des diffﾃｩrents ﾃｩlﾃｩments

public class View extends GUIState {

    public static int FRAME_SIZE = 600;
    public Display2D display;
    public JFrame displayFrame;
    SparseGridPortrayal2D yardPortrayal; //pour avoir plusieurs ﾃｩlﾃｩments dans une mﾃｪme cellule


    public View(SimState state, SparseGridPortrayal2D yardPortrayal) {
        super(state);
        this.yardPortrayal = yardPortrayal;
    }

    public static String getName() {
        return "Simulation Asterix et Obelix";
    }

    public void start() {
        super.start();
        setupPortrayals();
    }

    public void load(SimState state) {
        super.load(state);
        setupPortrayals();
    }

    public void init(Controller c) {
        super.init(c);
        display = new Display2D(FRAME_SIZE, FRAME_SIZE, this);
        display.setClipping(false);
        displayFrame = display.createFrame();
        displayFrame.setTitle(View.getName());
        c.registerFrame(displayFrame); //pour que le frame apparaisse dans la liste display
        displayFrame.setVisible(true);
        display.attach(yardPortrayal, "Yard");
    }

    public void setupPortrayals() { // Default
        //Elements qui se deplacent
        Lambdas lambdas = (Lambdas) state;

        yardPortrayal.setField(lambdas.yard);

        yardPortrayal.setPortrayalForClass(RomainLambda.class, getTypeCPortrayal());
        yardPortrayal.setPortrayalForClass(GauloisLambda.class, getTypeAPortrayal());
        yardPortrayal.setPortrayalForClass(Food.class, getTypeBPortrayal());
        yardPortrayal.setPortrayalForClass(Ingredient.class, getTypeEPortrayal());
        yardPortrayal.setPortrayalForClass(Forest.class, getForestCellsPortrayal());
        yardPortrayal.setPortrayalForClass(CampG.class, getCampGCellsPortrayal());
        yardPortrayal.setPortrayalForClass(CampR.class, getCampRCellsPortrayal());
        yardPortrayal.setPortrayalForClass(Panoramix.class, getPanoramixPortrayal());
        yardPortrayal.setPortrayalForClass(Abraracourcix.class,getAbraracourcixPortrayal());
        yardPortrayal.setPortrayalForClass(Asterix.class,getAsterixPortrayal());
        yardPortrayal.setPortrayalForClass(Obelix.class,getObelixPortrayal());
        yardPortrayal.setPortrayalForClass(Cesar.class,getCesarPortrayal());
        yardPortrayal.setPortrayalForClass(ZoneAbra.class,getZoneAbraPortrayal());
        yardPortrayal.setPortrayalForClass(ZonePano.class,getZonePanoPortrayal());
        yardPortrayal.setPortrayalForClass(Banquet.class,getBanquetPortrayal());
        yardPortrayal.setPortrayalForClass(Menhir.class,getMenhirPortrayal());
        yardPortrayal.setPortrayalForClass(ZoneMenhir.class,getZoneMenhirPortrayal());



        display.reset(); // reschedule the displayer
        display.setBackdrop(Color.decode("#EBC695"));
        display.repaint(); // redraw the display
    }

    private Portrayal getTypeAPortrayal() { //Représentation d'un gaulois lambda
        OvalPortrayal2D r = new OvalPortrayal2D();
        r.paint = Color.decode(("#F27100"));
        r.filled = true;
        return r;
    }
    
    private Portrayal getTypeBPortrayal() { //rpz de la nourriture
        OvalPortrayal2D r = new OvalPortrayal2D();
        r.scale = 0.9;
        r.paint = Color.decode("#F164FF");
        r.filled = true;
        return r;
    }
    
    private Portrayal getTypeCPortrayal() { //Représentation d'un romain lambda
        OvalPortrayal2D r = new OvalPortrayal2D();
        r.paint = Color.decode("#000000");
        r.filled = true;
        return r;
    }


    private Portrayal getTypeDPortrayal() { //rpz du materiel
        OvalPortrayal2D r = new OvalPortrayal2D();
        r.paint = Color.decode("#909090");
        r.filled = true;
        return r;
    }

    private Portrayal getTypeEPortrayal() { //rpz des ingredients
        RectanglePortrayal2D r = new RectanglePortrayal2D();
        r.paint = Color.decode("#00F306");
        r.filled = true;
        return r;
    }

    private Portrayal getPanoramixPortrayal(){
        ImagePortrayal2D r = new ImagePortrayal2D(new ImageIcon("./rsc/panoramix.png"));
        r.scale = 2.75;
        return r;
    }

    private Portrayal getAsterixPortrayal(){
        ImagePortrayal2D r = new ImagePortrayal2D(new ImageIcon("./rsc/asterix.png"));
        r.scale = 2.75;
        return r;
    }

    private Portrayal getAbraracourcixPortrayal(){
        ImagePortrayal2D r = new ImagePortrayal2D(new ImageIcon("./rsc/abraracourcix.png"));
        r.scale = 3;
        return r;
    }

    private Portrayal getObelixPortrayal(){
        ImagePortrayal2D r = new ImagePortrayal2D(new ImageIcon("./rsc/obelix.png"));
        r.scale = 2.75;
        return r;
    }

    private Portrayal getAssuranceTourixPortrayal(){
        ImagePortrayal2D r = new ImagePortrayal2D(new ImageIcon("./rsc/assurancetourix.png"));
        r.scale = 2.75;
        return r;
    }

    private Portrayal getCesarPortrayal(){
        ImagePortrayal2D r = new ImagePortrayal2D(new ImageIcon("./rsc/cesar.png"));
        r.scale = 2.75;
        return r;
    }
    
    private Portrayal getForestCellsPortrayal() { //rpz des forest cells
        RectanglePortrayal2D r = new RectanglePortrayal2D();
        r.paint = Color.decode("#358900");
        r.filled = true;
        return r;
    }

    private Portrayal getCampGCellsPortrayal() { //rpz des CampG cells
        RectanglePortrayal2D r = new RectanglePortrayal2D();
        r.paint = Color.decode("#A14C00");
        r.filled = true;
        return r;
    }

    private Portrayal getCampRCellsPortrayal() { //rpz des CampR cells
        RectanglePortrayal2D r = new RectanglePortrayal2D();
        r.paint = Color.decode(("#2798CC"));
        r.filled = true;
        return r;
    }

    private Portrayal getZoneAbraPortrayal() { //rpz des CampR cells
        RectanglePortrayal2D r = new RectanglePortrayal2D();
        r.paint = Color.LIGHT_GRAY;
        r.filled = true;
        return r;
    }

    private Portrayal getZonePanoPortrayal() { //rpz des CampR cells
        RectanglePortrayal2D r = new RectanglePortrayal2D();
        r.paint = Color.DARK_GRAY;
        r.filled = true;
        return r;
    }

    private Portrayal getBanquetPortrayal() { //rpz des CampR cells
        RectanglePortrayal2D r = new RectanglePortrayal2D();
        r.paint = Color.PINK;
        r.filled = true;
        return r;
    }

    private Portrayal getMenhirPortrayal() { //rpz des CampR cells
        ImagePortrayal2D r = new ImagePortrayal2D(new ImageIcon(Constants.imageMenhir));
        r.scale = 4;
        return r;
    }

    private Portrayal getZoneMenhirPortrayal() { //rpz des CampR cells
        RectanglePortrayal2D r = new RectanglePortrayal2D();
        r.paint = Color.LIGHT_GRAY;
        r.filled = true;
        return r;
    }
    
}


