package personnagesSpeciaux.Gaulois;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.ImageIcon;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import constants.Constants;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import lambda.GauloisLambda;
import lambda.Lambdas;
import pathfinding.AStar;
import ressources.Food;
import sim.engine.SimState;
import sim.portrayal.Portrayal;
import sim.portrayal.simple.ImagePortrayal2D;
import sim.util.Int2D;
import zones.campGaulois;
import zones.champDeBataille;

// Singleton
public class Abraracourcix extends GauloisLambda {

	private static final long serialVersionUID = 1L;
	private static Abraracourcix INSTANCE = null;
	private int nbGauloisEnChasse = 0;
	private int nbSangliers = 0; // Nombre de sanglier que Abra pense qu'il y ait au camp
	private int periodeVie = 0;
	private List<Food> sangliersDansForet = new ArrayList<Food>();
    public static Abraracourcix getInstance() {
        return INSTANCE;
    }
	
	@Override
	public void setup() {
		INSTANCE = this;
		super.setup();
		addBehaviour(new EcouteGLDemandeDeTache());
		addBehaviour(new EcouteGLTacheTermine());
		addBehaviour(new EcouteCesar());
	}
	
	// ----------BEGIN -- GETTER & SETTER -- BEGIN----------//
	
	public void setSangliersDansForet(List<Food> sangliersDansForet) {
		this.sangliersDansForet = sangliersDansForet;
	}
	
	public List<Food> getSangliersDansForet() {
		return sangliersDansForet;
	}
	
	public int getNbGauloisEnChasse() {
		return nbGauloisEnChasse;
	}

	public void setNbGauloisEnChasse(int nbGauloisEnChasse) {
		this.nbGauloisEnChasse = nbGauloisEnChasse;
	}
	
	public int getNbSangliers() {
		return nbSangliers;
	}

	public void setNbSangliers(int nbSangliers) {
		this.nbSangliers = nbSangliers;
	}
	// ------------END -- GETTER & SETTER -- END------------//
	// ------------BEGIN -- ESPACE JADE -- BEGIN------------//
	
	protected class CheckSanglier extends OneShotBehaviour { // Envoi une ressource ï¿½ï½¿ï½½ si on peut l'envoyer
		// dans un camps
		private static final long serialVersionUID = 1L;

		public CheckSanglier() {
			ACLMessage demandeCamp = new ACLMessage(Constants.AbraCheckStock);
			demandeCamp.addReceiver(Constants.campGaulois);
			send(demandeCamp);
		}

		public void action() {
			MessageTemplate mt = MessageTemplate.MatchSender(Constants.campGaulois);
			ACLMessage recu = receive(mt);
			if(recu != null) {
				nbSangliers = Integer.parseInt(recu.getContent());
			}
			else {
				block();
			}
		}
	}
	
	protected class EcouteCesar extends CyclicBehaviour { // Envoi une ressource ï¿½ï½¿ï½½ si on peut l'envoyer
		private static final long serialVersionUID = 1L;

		public void action() {
			MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchPerformative(Constants.OrdreBataille),MessageTemplate.MatchSender(Constants.cesar));
			ACLMessage recu = receive(mt);
			if(recu != null) {
				ordreBataille();
				setEnGuerre(true);
			}
			else {
				block();
			}
		}
		
	}
	
	protected class EcouteGLDemandeDeTache extends CyclicBehaviour { // Envoi une ressource ï¿½ï½¿ï½½ si on peut l'envoyer
		// dans un camps
		private static final long serialVersionUID = 1L;

		public EcouteGLDemandeDeTache() {}

		public void action() {
			MessageTemplate mt = MessageTemplate.MatchPerformative(Constants.GLDemandeDeTache);
			ACLMessage recu = receive(mt);
			if(recu != null) {
				ACLMessage reply = recu.createReply();
				if(nbSangliers + nbGauloisEnChasse < Constants.CAPACITE_SANGLIER_CAMP && getSangliersDansForet().size() > 0) {
					nbGauloisEnChasse++;
					Food sanglier = getSangliersDansForet().get(0);
					if(sanglier != null) {
						reply.setContent(toJSON(sanglier.getPosition()));
						getSangliersDansForet().remove(0);
						
					}
					else {
						reply.setContent("0");
					}
				}
				else {
					reply.setContent("0");
				}
				send(reply);
			}
			else {
				block();
			}
		}
		
	}
	
	protected class EcouteGLTacheTermine extends CyclicBehaviour { // Envoi une ressource ï¿½ï½¿ï½½ si on peut l'envoyer
		// dans un camps
		private static final long serialVersionUID = 1L;

		public EcouteGLTacheTermine() {}

		public void action() {
			MessageTemplate mt = MessageTemplate.MatchPerformative(Constants.GLTacheTermine);
			ACLMessage recu = receive(mt);
			if(nbGauloisEnChasse > 0) {
				if(recu != null) {
					nbGauloisEnChasse--;
					nbSangliers++;
				}
				else {
					block();
				}
			}
		}
	}
	// --------------END -- ESPACE JADE -- END--------------//

	@Override
	public void step(SimState state) {
		// TODO Cycle de vie Abraracourcix
		if(isEnGuerre()) {
			if(getEtapeBataille() == Constants.DEBUT_GUERRE) {
				this.setNbGauloisEnChasse(0);
				setEtapeBataille(Constants.INIT_POSITION_SUR_CHAMP);
			}
			else if(this.getEtapeBataille() == Constants.INIT_POSITION_SUR_CHAMP) {
				if(campGaulois.getInstance().getNbGauloisLambda() == 0) {
					this.setEtapeBataille(Constants.SE_BATTRE);
				}
			}
			else if(this.getEtapeBataille() == Constants.SE_BATTRE) {
				//pour attendre que tout le monde soit rentré au camp après la bataille
				if(campGaulois.getInstance().getNbGauloisLambda() == campGaulois.getInstance().getNbGauloisLambdaRestant() && campGaulois.getInstance().isAsterixHere() && campGaulois.getInstance().isObelixHere()) {
					champDeBataille.getInstance().setNbRomainsLambda(0);
					System.out.println("Cassez vous");
					this.setEnGuerre(false);
					this.setEtapeBataille(Constants.DEBUT_GUERRE);
				}
			}
		}
		else {
			if(periodeVie % Constants.PERIODE_CHECK_SANGLIER_CAMP == 0 ) {
				addBehaviour(new CheckSanglier());
				periodeVie = 0;
			}
			periodeVie++;
		}
		seBalader();
		updatePortrayal();
	}
	
	@Override
	protected void seBalader() {
		// TODO Aller chercher un sanglier en for�t
		if (chemin == null) { // On va vers le sanglier
			Random r = new Random();
			nPositionBalade = 1 + r.nextInt(Constants.MAX_BALADE);
			positionToGo = Lambdas.getInstance().getNewPosAbra();
			chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
		}
		
		if (position == positionToGo) { // On est sur le sanglier OU on est rentr�e au camp
			if(nPositionBalade > 0) {
				positionToGo = Lambdas.getInstance().getNewPosAbra();
				chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
				nPositionBalade--;
			}
			else {
				this.setEnBalade(false);
				chemin = null;
			}
		} else {
			caseSuivante();
		}
	}
	
	@Override
	protected Portrayal getObjectPortrayal() {
		ImagePortrayal2D r = new ImagePortrayal2D(new ImageIcon(Constants.imageAbraracourcix));
        r.scale = 2.75;
        return r;
	}
	
	public void ordreBataille() {
		ACLMessage ordreBataille = new ACLMessage(Constants.OrdreBataille);
		for(int nGaulois = 0; nGaulois < Constants.N_GAULOIS_LAMBDA; nGaulois++) {
			ordreBataille.addReceiver(new AID("GauloisLambda"+nGaulois, AID.ISLOCALNAME));
		}
		ordreBataille.addReceiver(Constants.asterix);
		ordreBataille.addReceiver(Constants.obelix);
		send(ordreBataille);
	}
	
	public static String toJSON(Int2D positionToGo) {
		ObjectMapper mapper = new ObjectMapper();
		String s = "";
		try {
			s = mapper.writeValueAsString(positionToGo);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return s;
	}
}
