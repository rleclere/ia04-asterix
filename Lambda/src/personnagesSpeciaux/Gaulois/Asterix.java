package personnagesSpeciaux.Gaulois;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import constants.Constants;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import lambda.GauloisLambda;
import lambda.Lambdas;
import lambda.RomainLambda;
import pathfinding.AStar;
import ressources.Ingredient;
import sim.engine.SimState;
import sim.portrayal.Portrayal;
import sim.portrayal.simple.ImagePortrayal2D;
import sim.util.Int2D;
import zones.campGaulois;
import zones.champDeBataille;

//Singleton
public class Asterix extends GauloisLambda {

	private static final long serialVersionUID = 1L;
	private int potionDansGourde = 0;
	private Map<String, Integer> ingredientsPotion = new HashMap<String, Integer>();
	private boolean chercheIngredientPotion = false;
	private List<Ingredient> guisDansForet = new ArrayList<Ingredient>();
	private static Asterix INSTANCE = null;
	private static List<RomainLambda> adversaires = new ArrayList<RomainLambda>();
	@Override
	protected void setup() {
		INSTANCE = this;
		super.setup();
		addBehaviour(new EcoutePanoramix());
		this.setPointDeVie(this.getPointDeVie() * 4);
		this.setPointAttaque(this.getPointAttaque()*Constants.BOOST_POTION_ASTERIX);
	}

	public static Asterix getInstance() {
		return INSTANCE;
	}

	// ----------BEGIN -- GETTER & SETTER -- BEGIN----------//

	public List<Ingredient> getGuisDansForet() {
		return guisDansForet;
	}

	public int getPotionDansGourde() {
		return potionDansGourde;
	}

	public void setPotionDansGourde(int potionDansGourde) {
		this.potionDansGourde = potionDansGourde;
	}

	public boolean isChercheIngredientPotion() {
		return chercheIngredientPotion;
	}

	public void setChercheIngredientPotion(boolean chercheIngredientPotion) {
		this.chercheIngredientPotion = chercheIngredientPotion;
	}
	// ------------END -- GETTER & SETTER -- END------------//
	// ------------BEGIN -- ESPACE JADE -- BEGIN------------//

	protected class EcoutePanoramix extends CyclicBehaviour { // Envoi une ressource �ｿｽ si on peut l'envoyer
		// dans un camps
		private static final long serialVersionUID = 1L;

		public EcoutePanoramix() {

		}

		public void action() {
			MessageTemplate mt = MessageTemplate.MatchSender(Constants.panoramix);
			ACLMessage recu = receive(mt);
			// TODO SEULEMENT SI DANS LE CAMP
			if (recu != null && !isChercheIngredientPotion()) {
				setChercheIngredientPotion(true);
				chemin = null;
			} else {
				block();
			}
		}

	}

	protected class RemplirGourde extends OneShotBehaviour { // Envoi une ressource �ｿｽ si on peut l'envoyer
		// dans un camps
		private static final long serialVersionUID = 1L;

		public RemplirGourde() {
			ACLMessage message = new ACLMessage(Constants.AsterixDemandeRemplirGourde);
			int potionsDemande = Constants.TAILLE_GOURDE_ASTERIX - potionDansGourde;
			message.setContent(potionsDemande + "");
			message.addReceiver(Constants.campGaulois);
			send(message);
		}

		public void action() {
			MessageTemplate mt = MessageTemplate.MatchSender(Constants.campGaulois);
			ACLMessage recu = receive(mt);
			if (recu != null) {
				int potionDonne = Integer.parseInt(recu.getContent());
				potionDansGourde += potionDonne;
			} else {
				block();
			}
		}
	}

	// --------------END -- ESPACE JADE -- END--------------//

	@Override
	public void step(SimState state) {
		// TODO Cycle de vie ASTERIX
		if (isBoostPotion()) {
			if (this.tempsPotionRestant == 0) {
				this.setBoostPotion(false);
				this.tempsPotionRestant = Constants.TEMPS_POTION;
			} else {
				this.tempsPotionRestant--;
			}
		}
		if (isEnGuerre()) {
			if(this.getEtapeBataille() == Constants.DEBUT_GUERRE) {
				initGuerre();
			}
			if (this.getEtapeBataille() == Constants.ALLER_BOIRE_POTION) {
				allerBoirePotion();
			}
			if (this.getEtapeBataille() == Constants.INIT_POSITION_SUR_CHAMP) {
				initPosition();
			}
			if (this.getEtapeBataille() == Constants.SE_BATTRE) {
				combattre();
			}
			if (this.getEtapeBataille() == Constants.BANQUET) {
				feterLaVictoire();
			}
			if (this.getEtapeBataille() == Constants.ATTENDRE_GAULOIS) {
				if (campGaulois.getInstance().getNbGauloisLambda() == campGaulois.getInstance()
						.getNbGauloisLambdaRestant() && campGaulois.getInstance().isAsterixHere() && campGaulois.getInstance().isObelixHere()) {
					this.setEnGuerre(false);
					this.setEtapeBataille(Constants.DEBUT_GUERRE);
				}
			}
		} else {
			if (isChercheIngredientPotion()) {
				allerChercherIngredientsPotion();
			} else if (this.isAllerChercherSanglier()) {
				allerChercherSanglier();
			} else if (pointDeVie < Constants.MAX_VIE_LAMBDA - Constants.SANGLIER) {
				// TODO si on est dans le bonne zone
				addBehaviour(new demandeFood());
			} else if (this.isAllerDemanderTache()) {
				allerDemanderTache();

			} else if(this.isEnBalade()) {
				seBalader();
			} else {
				this.setAllerDemanderTache(true);
			}
		}
		updatePortrayal();
	}
	
	@Override
	protected void combattre() {
		if (adversaire.getPointDeVie() > 0) {
			if (mettreUnCoup) {
				this.mettreUnCoup(adversaire);
			} else {
				if (voirEnnemiEnFace(new Int2D(this.getPosition().getX() - 1, this.getPosition().getY()),
						RomainLambda.class) != adversaire) {
					if (chemin == null) {
						positionToGo = new Int2D(adversaire.getPosition().x + 1, adversaire.getPosition().getY());
						chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
					}

				} 
				if (chemin != null) {
					if (position == positionToGo) {
						mettreUnCoup = true;
						chemin = null;
					} else {
						caseSuivante();
					}
				}
			}
		} else {
			if(adversaires.size() > 0) {
				adversaire = adversaires.get(0);
				adversaires.remove(0);
			}
			else {
				adversaire = null;
				this.setEtapeBataille(Constants.BANQUET);
			}
			chemin = null;
		}
	}
	
	@Override
	protected void initPosition() {
		if (chemin == null) {
			// TODO MODIFICATION ICI A FAIRE
			for(int nRomains = Constants.N_ROMAINS_LAMBDA - Constants.N_ADVERSAIRES_ASTERIX; nRomains < Constants.N_ROMAINS_LAMBDA; nRomains++) {
				if(adversaire == null) {
					adversaire = champDeBataille.getInstance().getRomainsReady().get(nRomains);
				}
				else {
					adversaires.add(champDeBataille.getInstance().getRomainsReady().get(nRomains));
				}
			}
			addBehaviour(new changementDeZone(this, Constants.campGaulois, Constants.champDeBataille));
			positionToGo = new Int2D(adversaire.getPosition().getX() + 1, adversaire.getPosition().getY());
			chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
		}
		if (position == positionToGo) {
			mettreUnCoup = true;
			chemin = null;
			this.setEtapeBataille(Constants.SE_BATTRE);
		} else {
			caseSuivante();
		}
	}
	
	@Override
	protected void initGuerre() {
		if(this.isChercheIngredientPotion()) {
			if(this.zonePosition(positionToGo).equals("foret")) { // Si on �tait en train de r�cup�rer les ingr�dients
				while(this.getGuisDansForet().size() > 0) {
					Ingredient gui = getGuisDansForet().get(0);
					guisDansForet.remove(gui);
					Lambdas.getInstance().getYard().remove(gui);
				}
				Lambdas.getInstance().addIngredients();
				ingredientsPotion =  new HashMap<String, Integer>();
				this.setEtapeBataille(Constants.ALLER_BOIRE_POTION);
				setChercheIngredientPotion(false);
			}
			else if(this.zonePosition(positionToGo).equals("campG")) { // Si on ramenait les ingr�dients, on les ram�ne quoi qu'il arrive
				if (chemin == null) {
					Int2D druide = Panoramix.getInstance().getPosition();
					positionToGo = new Int2D(druide.getX(), druide.getY() - 1);
					chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
				}
				
				if (position == positionToGo) {
					donnerIngredientsPanoramix();
					setChercheIngredientPotion(false);
					this.setEtapeBataille(Constants.ALLER_BOIRE_POTION);
					chemin = null;
					fatigue();
				} else {
					caseSuivante();
				}
			}
		}
		else {
			super.initGuerre();
		}
	}
	
	@Override
	protected void demanderPotionPano() {
		System.out.println("Demander potion");
		addBehaviour(new boirePotion());
		addBehaviour(new RemplirGourde());
	}

	@Override
	 protected Portrayal getObjectPortrayal(){
	        ImagePortrayal2D r = new ImagePortrayal2D(new ImageIcon(Constants.imageAsterix) );
	        r.scale = 2.75;
	        return r;
    }

	
	private void allerChercherIngredientsPotion() {
		if (chemin == null) {
			if (getGuisDansForet().size() > 0) { // On va au gui suivant
				positionToGo = getGuisDansForet().get(0).getPosition();
			} else { // On peut retourner voir panoramix
				Int2D druide = Panoramix.getInstance().getPosition();
				positionToGo = new Int2D(druide.getX(), druide.getY() - 1);
			}
			chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
		}

		if (position == positionToGo) {
			if (zoneDePresence().equals("foret")) {
				Ingredient gui = getGuisDansForet().get(0);
				if (ingredientsPotion.get("gui") == null) {
					ingredientsPotion.put("gui", 1);
				} else {
					ingredientsPotion.put("gui", ingredientsPotion.get("gui") + 1);
				}
				guisDansForet.remove(gui);
				Lambdas.getInstance().getYard().remove(gui);
			} else if (zoneDePresence().equals("campG")) {
				donnerIngredientsPanoramix();
				setChercheIngredientPotion(false);
			}
			
			if (getGuisDansForet().size() > 0) { // On va au gui suivant
				positionToGo = getGuisDansForet().get(0).getPosition();
			} else { // On peut retourner voir panoramix
				Int2D druide = Panoramix.getInstance().getPosition();
				positionToGo = new Int2D(druide.getX(), druide.getY() - 1);
			}
			chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
			
			fatigue();
		} else {
			caseSuivante();
		}
	}

	public void boireGourde() {
		if (this.getPotionDansGourde() > 0) {
			this.potionDansGourde--;
			this.setBoostPotion(true);
		}
	}

	public void donnerIngredientsPanoramix() {
		// TODO sch�ｿｽma �ｿｽ suivre
		ACLMessage envoyerIngredientsPano = new ACLMessage(Constants.AsterixDonneIngredientsPano);
		envoyerIngredientsPano.setContent(toJSON(this.ingredientsPotion));
		envoyerIngredientsPano.addReceiver(Constants.panoramix);
		send(envoyerIngredientsPano);
		this.setChercheIngredientPotion(false);
	}

	public static String toJSON(Map<String, Integer> ingredients) {
		ObjectMapper mapper = new ObjectMapper();
		String s = "";
		try {
			s = mapper.writeValueAsString(ingredients);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return s;
	}

}
