package personnagesSpeciaux.Gaulois;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.ImageIcon;

import constants.Constants;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import lambda.GauloisLambda;
import lambda.Lambdas;
import lambda.RomainLambda;
import pathfinding.AStar;
import ressources.Food;
import sim.engine.SimState;
import sim.portrayal.Portrayal;
import sim.portrayal.simple.ImagePortrayal2D;
import sim.util.Int2D;
import zones.campGaulois;
import zones.champDeBataille;

// Singleton
public class Obelix extends GauloisLambda {

	private static final long serialVersionUID = 1L;
	private static Obelix INSTANCE = null;
	private boolean enProductionMehnir = false;
	private int tempsPreparationRestant = Constants.TEMPS_PREPARATION_MENHIR;
	private int periodeVie = 0;
	private boolean menhirPret = false;
	private static List<RomainLambda> adversaires = new ArrayList<RomainLambda>();
	
	public static Obelix getInstance()
	{
		return INSTANCE;
	}

	@Override
	protected void setup() {
		INSTANCE = this;
		super.setup();
		this.setBoostPotion(true); //toujours la potion dans le sang
		this.setPointAttaque(this.getPointAttaque()*Constants.BOOST_POTION_OBELIX);
	}
	// ----------BEGIN -- GETTER & SETTER -- BEGIN----------//
	public boolean isenProductionMehnir() {
		return enProductionMehnir;
	}

	public void setenProductionMehnir(boolean enProductionMehnir) {
		this.enProductionMehnir = enProductionMehnir;
	}
	
	public int getTempsPreparationRestant() {
		return tempsPreparationRestant;
	}

	public void setTempsPreparationRestant(int tempsPreparationRestant) {
		this.tempsPreparationRestant = tempsPreparationRestant;
	}
	
	public boolean isMenhirPret() {
		return menhirPret;
	}

	public void setMenhirPret(boolean menhirPret) {
		this.menhirPret = menhirPret;
	}
	// ------------END -- GETTER & SETTER -- END------------//
	// ------------BEGIN -- ESPACE JADE -- BEGIN------------//
	// --------------END -- ESPACE JADE -- END--------------//

	@Override
	public void step(SimState state) {
		// TODO Cycle de vie Obelix
		if (isEnGuerre()) {
			if(this.getEtapeBataille() == Constants.DEBUT_GUERRE) {
				initGuerre();
			}
			if (this.getEtapeBataille() == Constants.ALLER_BOIRE_POTION) {
				this.setEtapeBataille(Constants.INIT_POSITION_SUR_CHAMP);
				chemin = null;
			}
			if (this.getEtapeBataille() == Constants.INIT_POSITION_SUR_CHAMP) {
				initPosition();
			}
			if (this.getEtapeBataille() == Constants.SE_BATTRE) {
				combattre();
			}
			if (this.getEtapeBataille() == Constants.BANQUET) {
				feterLaVictoire();
				//comportement diff pour manger plus
			}
			if (this.getEtapeBataille() == Constants.ATTENDRE_GAULOIS) {
				if (campGaulois.getInstance().getNbGauloisLambda() == campGaulois.getInstance()
						.getNbGauloisLambdaRestant() && campGaulois.getInstance().isAsterixHere() && campGaulois.getInstance().isObelixHere()) {
					this.setEnGuerre(false);
					this.setEtapeBataille(Constants.DEBUT_GUERRE);
				}
			}
		}
		else {
			if(periodeVie % Constants.PERIODE_CHECK_MENHIR_CAMP == 0 && !isenProductionMehnir()) {
				addBehaviour(new checkMenhir());
				periodeVie = 0;
			} else if(this.isenProductionMehnir()) {
				produireMenhirs();
			}
			else if (this.isAllerChercherSanglier()) {
				allerChercherSanglier();
			} else if (pointDeVie < Constants.MAX_VIE_LAMBDA - Constants.SANGLIER) {
				// TODO si on est dans le bonne zone
				addBehaviour(new demandeFood());
			} else if (this.isAllerDemanderTache()) {
				allerDemanderTache();

			} else if(this.isEnBalade()) {
				seBalader();
			} else {
				this.setAllerDemanderTache(true);
			}
			if(!isenProductionMehnir()) {
				periodeVie++;
			}
		}
		updatePortrayal();
	}
	
	@Override
	protected void combattre() {
		if (adversaire.getPointDeVie() > 0) {
			if (mettreUnCoup) {
				this.mettreUnCoup(adversaire);
			} else {
				if (voirEnnemiEnFace(new Int2D(this.getPosition().getX() - 1, this.getPosition().getY()),
						RomainLambda.class) != adversaire) {
					if (chemin == null) {
						positionToGo = new Int2D(adversaire.getPosition().x + 1, adversaire.getPosition().getY());
						chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
					}
				} 
				if (chemin != null) {
					if (position == positionToGo) {
						mettreUnCoup = true;
						chemin = null;
					} else {
						caseSuivante();
					}
				}
			}
		} else {
			if(adversaires.size() > 0) {
				adversaire = adversaires.get(0);
				adversaires.remove(0);
			}
			else {
				adversaire = null;
				this.setEtapeBataille(Constants.BANQUET);
			}
			chemin = null;
		}
	}
	
	protected void initPosition() {
		if (chemin == null) {
			// TODO MODIFICATION ICI A FAIRE
			for(int nRomains = Constants.N_ROMAINS_LAMBDA - Constants.N_ADVERSAIRES_OBELIX - Constants.N_ADVERSAIRES_ASTERIX
					; nRomains < Constants.N_ROMAINS_LAMBDA - Constants.N_ADVERSAIRES_ASTERIX; nRomains++) {
				if(adversaire == null) {
					adversaire = champDeBataille.getInstance().getRomainsReady().get(nRomains);
				}
				else {
					adversaires.add(champDeBataille.getInstance().getRomainsReady().get(nRomains));
				}
			}
			addBehaviour(new changementDeZone(this, Constants.campGaulois, Constants.champDeBataille));
			positionToGo = new Int2D(adversaire.getPosition().getX() + 1, adversaire.getPosition().getY());
			chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
		}
		if (position == positionToGo) {
			mettreUnCoup = true;
			chemin = null;
			this.setEtapeBataille(Constants.SE_BATTRE);
		} else {
			caseSuivante();
		}
	}
	
	protected Portrayal getObjectPortrayal(){
        ImagePortrayal2D r = new ImagePortrayal2D(new ImageIcon(Constants.imageObelix));
        r.scale = 2.75;
        return r;
    }
	
	protected class checkMenhir extends Behaviour { // Envoi une ressource ï¿½ si on peut l'envoyer
		// dans un camps
		private static final long serialVersionUID = 1L;
		private boolean test = false; //reception du msg

		public checkMenhir() {
			ACLMessage demandeCamp = new ACLMessage(Constants.ObelixCheckMenhir);
			demandeCamp.addReceiver(Constants.campGaulois);
			send(demandeCamp);
		}

		public void action() {
			MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchSender(Constants.campGaulois), MessageTemplate.MatchPerformative(Constants.ObelixCheckMenhir));
			ACLMessage recu = receive(mt);
			if(recu != null) {
				test = true;
				int nbMenhirs = Integer.parseInt(recu.getContent());
				if(nbMenhirs < Constants.MIN_MEHNIRS_DANS_CAMP) {
					setenProductionMehnir(true);
					chemin = null;
				}
			}
			else {
				block();
			}
		}

		@Override
		public boolean done() {
			return test;
		}
	}
	
	public void produireMenhirs() {
		if(chemin == null) {
			if (this.isAllerChercherSanglier() && zonePosition(positionToGo).equals("foret")) {
				// Si on allait chercher un sanglier, on prévient Abraracourcix qu'on est pas
				// allé le chercher
				Food sanglier = new Food(positionToGo, "Sanglier", Constants.LocationType.Forest);
				Abraracourcix.getInstance().getSangliersDansForet().add(sanglier);
			}
			this.setAllerChercherSanglier(false);
			positionToGo = Lambdas.getInstance().getNewPosMehnir();
			chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
		}
		if (position == positionToGo) { // On est sur le sanglier OU on est rentr�e au camp
			if(this.tempsPreparationRestant == 0) { // Fin de la pré§±aration
				donnerMenhirCamp();
				this.setTempsPreparationRestant(Constants.TEMPS_PREPARATION_MENHIR);
				this.setenProductionMehnir(false);
				chemin = null;
				
			} else { // On doit encore attendre
				this.tempsPreparationRestant--;
			}
		} else {
			caseSuivante();
		}
	}
	
	public void donnerMenhirCamp() {
		ACLMessage menhir = new ACLMessage(Constants.ObelixDonneMenhir);
		menhir.addReceiver(Constants.campGaulois);
		send(menhir);
	}
	
	protected class demandeMenhir extends OneShotBehaviour { // Envoi une ressource ï¿½ si on peut l'envoyer
		// dans un camps
		private static final long serialVersionUID = 1L;

		public demandeMenhir() {
			ACLMessage demandeCamp = new ACLMessage(Constants.ObelixDemandeMenhir);
			demandeCamp.addReceiver(Constants.campGaulois);
			send(demandeCamp);
		}

		public void action() {
			MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchSender(Constants.campGaulois), MessageTemplate.MatchPerformative(Constants.ObelixDemandeMenhir));
			ACLMessage recu = receive(mt);
			if(recu != null) {
				if(recu.getContent().equals("1")) {
					setMenhirPret(true);
				}
			}
			else {
				block();
			}
		}
	}
	
	
	
	
}
