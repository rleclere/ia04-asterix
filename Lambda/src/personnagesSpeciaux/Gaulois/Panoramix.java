package personnagesSpeciaux.Gaulois;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.swing.ImageIcon;

import com.fasterxml.jackson.databind.ObjectMapper;

import constants.Constants;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import lambda.GauloisLambda;
import lambda.Lambdas;
import pathfinding.AStar;
import sim.engine.SimState;
import sim.portrayal.Portrayal;
import sim.portrayal.simple.ImagePortrayal2D;
import sim.util.Int2D;

// Singleton
public class Panoramix extends GauloisLambda { 

	private static final long serialVersionUID = 1L;
	private int tempsPreparationRestant = Constants.TEMPS_PREPARATION_POTION;
	private boolean enPreparation = false;
	private static Panoramix INSTANCE = null;
	private Map<String,Integer> ingredients = null;
	private int periodeVie = 0;
	
	@Override
	public void setup() {
		INSTANCE = this;
		super.setup();
		addBehaviour(new ecouteAsterix());
	}
	

	public static Panoramix getInstance()
	{
		return INSTANCE;
	}


	// ----------BEGIN -- GETTER & SETTER -- BEGIN----------//

	public boolean isEnPreparation() {
		return enPreparation;
	}

	public void setEnPreparation(boolean enPreparation) {
		this.enPreparation = enPreparation;
	}

	public int getTempsPreparationRestant() {
		return tempsPreparationRestant;
	}

	public void setTempsPreparationRestant(int tempsPreparationRestant) {
		this.tempsPreparationRestant = tempsPreparationRestant;
	}
	
	public Map<String,Integer> getIngredients() {
		return ingredients;
	}

	public void setIngredients(Map<String,Integer> ingredients) {
		this.ingredients = ingredients;
	}

	// ------------END -- GETTER & SETTER -- END------------//
	// ------------BEGIN -- ESPACE JADE -- BEGIN------------//
	// Demande ï¿½ï½¿ï½½ asterix les ingrï¿½ï½¿ï½½dients pour faire la potion
	// Reception d'une demande pour boire de la potion ?
	// --------------END -- ESPACE JADE -- END--------------//

	@Override
	public void step(SimState state) {
		// TODO Cycle de vie PANORAMIX
		if(periodeVie % Constants.PERIODE_CHECK_POTION_CAMP == 0 && !isEnPreparation()) {
			addBehaviour(new checkPotion());
			periodeVie = 0;
		}
		periodeVie++;
		produirePotionMagique();
		seBalader();
		updatePortrayal();
	}
	
	@Override
	protected void seBalader() {
		// TODO Aller chercher un sanglier en for�t
		if (chemin == null) { // On va vers le sanglier
			Random r = new Random();
			nPositionBalade = 1 + r.nextInt(Constants.MAX_BALADE);
			positionToGo = Lambdas.getInstance().getNewPosPano();
			chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
		}
		
		if (position == positionToGo) { // On est sur le sanglier OU on est rentr�e au camp
			if(nPositionBalade > 0) {
				positionToGo = Lambdas.getInstance().getNewPosPano();
				chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
				nPositionBalade--;
			}
			else {
				this.setEnBalade(false);
				chemin = null;
			}
		} else {
			caseSuivante();
		}
	}
	
	@Override
	protected Portrayal getObjectPortrayal() {
		ImagePortrayal2D r = new ImagePortrayal2D(new ImageIcon(Constants.imagePanoramix));
        r.scale = 2.75;
        return r;
	}
	
	public void demandeIngredientAsterix() {
		ACLMessage message = new ACLMessage(Constants.PanoDemandeIngredientsAsterix);
		message.addReceiver(Constants.asterix);
		send(message);
	}

	public void produirePotionMagique() {
		if(this.isEnPreparation()) { // Prï¿½ï½¿ï½½paration en cours
			if(this.tempsPreparationRestant == 0) { // Fin de la prï¿½ï½¿ï½½paration
				remplirChaudronCamp();
				this.setTempsPreparationRestant(Constants.TEMPS_PREPARATION_POTION);
				this.setEnPreparation(false);
				setIngredients(null);
				Lambdas.getInstance().addIngredients();
			} else { // On doit encore attendre
				this.tempsPreparationRestant--;
			}
		}
	}
	
	public void remplirChaudronCamp() {
		ACLMessage chaudronPlein = new ACLMessage(Constants.PanoDonnePotion);
		chaudronPlein.addReceiver(Constants.campGaulois);
		send(chaudronPlein);
	}

	protected class ecouteAsterix extends CyclicBehaviour {
		// dans un camps
		private static final long serialVersionUID = 1L;

		public ecouteAsterix() {

		}

		public void action() {
			MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchSender(Constants.asterix),MessageTemplate.MatchPerformative(Constants.AsterixDonneIngredientsPano));
			ACLMessage recu = receive(mt);
			if(!isEnPreparation()) {
				if(recu != null) {
					setIngredients(fromJSON(recu.getContent()));
					setEnPreparation(true);
				}
				else {
					block();
				}
			}
		}
		
		public Map<String,Integer> fromJSON(String chaineList) {
			ObjectMapper mapper = new ObjectMapper();
			Map<String,Integer> s = new HashMap<String,Integer>();
			try {
				// TODO a tester
				s = mapper.readValue(chaineList, Map.class);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return s;
		}
	}
	
	protected class checkPotion extends Behaviour { // Envoi une ressource ï¿½ï½¿ï½½ si on peut l'envoyer
		// dans un camps
		private static final long serialVersionUID = 1L;
		boolean test = false;
		public checkPotion() {
			ACLMessage demandeCamp = new ACLMessage(Constants.PanoCheckStock);
			demandeCamp.addReceiver(Constants.campGaulois);
			send(demandeCamp);
		}

		public void action() {
			MessageTemplate mt = MessageTemplate.MatchSender(Constants.campGaulois);
			ACLMessage recu = receive(mt);
			if(recu != null) {
				int nbPotions = Integer.parseInt(recu.getContent());
				if(nbPotions < Constants.MIN_POTION_DANS_CAMP) {
					demandeIngredientAsterix();
				}
				test = true;
			}
			else {
				block();
			}
		}

		@Override
		public boolean done() {
			// TODO Auto-generated method stub
			return test;
		}
	}
}
