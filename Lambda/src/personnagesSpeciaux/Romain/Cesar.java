package personnagesSpeciaux.Romain;

import java.util.ArrayList;

import javax.swing.ImageIcon;

import constants.Constants;
import jade.core.AID;
import jade.lang.acl.ACLMessage;
import lambda.Lambdas;
import lambda.RomainLambda;
import sim.engine.SimState;
import sim.portrayal.Portrayal;
import sim.portrayal.simple.ImagePortrayal2D;
import zones.campRomain;
import zones.champDeBataille;

// Singleton
public class Cesar extends RomainLambda {

	private static final long serialVersionUID = 1L;
	private static Cesar INSTANCE = null;
	private int tempsRestantAvantBataille = Constants.STEP_AVANT_BATAILLE;
	private int tempo = 20;
	public static Cesar getInstance()
	{
		return INSTANCE;
	}
	
	@Override
	public void setup() {
		INSTANCE = this;
		super.setup();
		updatePortrayal();
	}
	
	// ----------BEGIN -- GETTER & SETTER -- BEGIN----------//
	// ------------END -- GETTER & SETTER -- END------------//
	// ------------BEGIN -- ESPACE JADE -- BEGIN------------//
	
	// Coordination des Romains, gestions des stocks
	
	// --------------END -- ESPACE JADE -- END--------------//

	@Override
	public void step(SimState state) {
		if(isEnGuerre()) {
			if(champDeBataille.getInstance().getNbRomainsLambda() == Constants.N_ROMAINS_LAMBDA && this.getEtapeBataille() == Constants.DEBUT_GUERRE) {
				declarerGuerre();
				this.setEtapeBataille(Constants.SE_BATTRE);
			} else if(this.getEtapeBataille() == Constants.SE_BATTRE && champDeBataille.getInstance().getNbRomainsLambda() == 0) {
				ACLMessage suppression = new ACLMessage(Constants.demandeDeSuppression);
				for(int i = 0; i < Constants.N_ROMAINS_LAMBDA; i++) {
					suppression.addReceiver(new AID("RomainLambda"+i,AID.ISLOCALNAME));
				}
				send(suppression);
				champDeBataille.getInstance().setAgentsPresent(new ArrayList<AID>());
				champDeBataille.getInstance().setRomainsReady(new ArrayList<RomainLambda>());
				campRomain.getInstance().setNbRomainsLambda(0);
				this.setEtapeBataille(Constants.NOUVELLE_GARNISON);
			} else if(this.getEtapeBataille() == Constants.NOUVELLE_GARNISON) {
				tempo--;
				if(tempo == 0) {
					tempo = 20;
					Lambdas.getInstance().addRomainsL();
					this.setEtapeBataille(Constants.DEBUT_GUERRE);
					this.setEnGuerre(false);
				}
			}
		} else {
			if(tempsRestantAvantBataille == 0) {
				ordreBataille();
				tempsRestantAvantBataille = Constants.STEP_AVANT_BATAILLE;
				this.setEnGuerre(true);
			}
			tempsRestantAvantBataille--;
		}
		seBalader();
		updatePortrayal();
	}
	
	@Override
	protected Portrayal getObjectPortrayal() {
		ImagePortrayal2D r = new ImagePortrayal2D(new ImageIcon(Constants.imageCesar));
        r.scale = 2.75;
        return r;
	}
	
	private void ordreBataille() {
		ACLMessage ordreBataille = new ACLMessage(Constants.OrdreBataille);
		for(int nRomain = 0; nRomain < Constants.N_ROMAINS_LAMBDA; nRomain++) {
			ordreBataille.addReceiver(new AID("RomainLambda"+nRomain, AID.ISLOCALNAME));
		}
		send(ordreBataille);
	}
	
	private void declarerGuerre() {
		ACLMessage declaration = new ACLMessage(Constants.OrdreBataille);
		declaration.addReceiver(Constants.abraracourcix);
		send(declaration);
		this.setEtapeBataille(Constants.SE_BATTRE);
	}
}
