package lambda;

import constants.Constants;
import main.MainBoot;
import personnagesSpeciaux.Gaulois.Abraracourcix;
import personnagesSpeciaux.Gaulois.Asterix;
import ressources.Food;
import ressources.Ingredient;
import sim.engine.SimState;
import sim.field.grid.SparseGrid2D;
import sim.portrayal.grid.SparseGridPortrayal2D;
import sim.util.Int2D;
import zones.*;

public class Lambdas extends SimState {

    private static final long serialVersionUID = 1L;
    public SparseGrid2D yard = new SparseGrid2D(Constants.GRID_SIZE, Constants.GRID_SIZE);
    private static Lambdas INSTANCE = null;
    private SparseGridPortrayal2D yardPortrayal = new SparseGridPortrayal2D();
    
    private Lambdas(long seed) {
        super(seed);
    }

	public static Lambdas getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Lambdas(System.currentTimeMillis()); // Position par d�ｿｽ�ｽｿ�ｽｽfaut
        }
        return INSTANCE;
    }
    public SparseGrid2D getYard() {
		return yard;
	}

	public void start() {
        super.start();
        yard.clear();
        addForestCells();
        addCampGCells();
        addCampRCells();
        addZoneAbraCells();
        addZonePanoCells();
        addZoneMenhirCells();
        addMenhir();
        addBanquetCells();
        addGauloisSpeciaux();
        addRomainsSpeciaux();
        addGauloisL();
        addRomainsL();
        
        addSangliers();
        addIngredients();
        //addMaterial();
        //addRations();
    }
    
	public SparseGridPortrayal2D getYardPortrayal() {
		return yardPortrayal;
	}

	public void setYardPortrayal(SparseGridPortrayal2D yardPortrayal) {
		this.yardPortrayal = yardPortrayal;
	}

    private void addGauloisL() {
        for (int i = 0; i < Constants.N_GAULOIS_LAMBDA; i++) {
            Int2D location = getNewPosGaulois(); //position de d�ｾ�ｽｩpart random pour les gaulois
            MainBoot.getInstance().addAgent("GauloisLambda"+i, "lambda.GauloisLambda", new Object[]{location});
        }
    }

    public void addRomainsL() {
        for (int i = 0; i < Constants.N_ROMAINS_LAMBDA; i++) {
            Int2D location = getNewPosRomain(); //position de d�ｾ�ｽｩpart random pour les romains
            MainBoot.getInstance().addAgent("RomainLambda"+i, "lambda.RomainLambda", new Object[]{location});
        }
    }

    public void addIngredients() {
        //TODO
        //Foret: Consumable cr�ｾ�ｽｩer un consumable de type sanglier/ingr�ｾ�ｽｩdients pour popo: gui cerfeuil champignons
        //Port: Materiel et consumable
        for (int i = 0; i < Constants.MAX_QUANTITY_INGREDIENT; i++) {
            Int2D location = getNewPosForet();
            Ingredient gui = new Ingredient(location, "Gui", Constants.LocationType.Forest);
            if(i < Constants.N_INGREDIENTS_POUR_CHAUDRON) {
            	Asterix.getInstance().getGuisDansForet().add(gui);
            }
            yard.setObjectLocation(gui, location);
            schedule.scheduleRepeating(gui);
        }
    }

    private void addSangliers() {
        for (int i = 0; i < Constants.MAX_QUANTITY_FOOD; i++) {
            Int2D location = getNewPosForet();
            Food sanglier = new Food(location, "Sanglier", Constants.LocationType.Forest);
            Abraracourcix.getInstance().getSangliersDansForet().add(sanglier);
            yard.setObjectLocation(sanglier, location);
            schedule.scheduleRepeating(sanglier);
        }
    }
    
    public void addNewSanglier() {
            Int2D location = getNewPosForet();
            Food sanglier = new Food(location, "Sanglier", Constants.LocationType.Forest);
            Abraracourcix.getInstance().getSangliersDansForet().add(sanglier);
            yard.setObjectLocation(sanglier, location);
            schedule.scheduleRepeating(sanglier);
    }

    /*private void addRations() {
        for (int i = 0; i < Constants.MAX_QUANTITY_FOOD; i++) {
            Int2D location = getNewPosPort();
            Food e = new Food(location, "Ration", Constants.LocationType.TradePort);
            yard.setObjectLocation(e, location);
            schedule.scheduleRepeating(e);
        }
    }*/

    /*private void addMaterial() {
        for (int i = 0; i < Constants.MAX_QUANTITY_MATERIAL; i++) {
            Int2D location = getNewPosPort();
            Material e = new Material(location, "Bois", Constants.LocationType.TradePort);
            yard.setObjectLocation(e, location);
            schedule.scheduleRepeating(e);

            Int2D location2 = getNewPosPort();
            Material e2 = new Material(location2, "Fer", Constants.LocationType.TradePort);
            yard.setObjectLocation(e2, location2);
            schedule.scheduleRepeating(e2);
        }
    }*/

    private void addForestCells() {
        for (int i = Constants.FORET_XMIN; i < Constants.FORET_XMAX; i++) {
            for (int j = Constants.FORET_YMIN; j < Constants.FORET_YMAX; j++) {
                Int2D location = new Int2D(i, j);
                Forest e = new Forest();
                yard.setObjectLocation(e, location);
            }
        }
    }

    /*private void addTradePortCells() {
        int limitX = Constants.GRID_SIZE / 3;
        int limitY = Constants.GRID_SIZE / 4;
        for (int i = limitX; i < 2 * limitX; i++) {
            for (int j = 0; j < limitY; j++) {
                Int2D location = new Int2D(i, j);
                TradePort e = new TradePort();
                yard.setObjectLocation(e, location);
            }
        }
    }*/

    private void addCampGCells() {
        for (int i = Constants.CAMPG_XMIN; i < Constants.CAMPG_XMAX; i++) {
            for (int j = Constants.CAMPG_YMIN; j < Constants.CAMPG_YMAX; j++) {
                Int2D location = new Int2D(i, j);
                CampG e = new CampG();
                yard.setObjectLocation(e, location);
            }
        }
    }

    private void addCampRCells() {
        for (int i = Constants.CAMPR_XMIN; i < Constants.CAMPR_XMAX; i++) {
            for (int j = Constants.CAMPR_YMIN; j < Constants.CAMPR_YMAX; j++) {
                Int2D location = new Int2D(i, j);
                CampR e = new CampR();
                yard.setObjectLocation(e, location);
            }
        }
    }

    private void addGauloisSpeciaux() {
        Int2D location = Constants.POSABRARACOURCIX;
        MainBoot.getInstance().addAgent(Constants.abraracourcix.getLocalName(), "personnagesSpeciaux.Gaulois.Abraracourcix", new Object[]{location});

        //location = getNewPosGaulois(); //position de d�ｾ�ｽｩpart random pour les gaulois
        //AssuranceTourix e1 = AssuranceTourix.getInstance(location);

        location = getNewPosGaulois(); //position de d�ｾ�ｽｩpart random pour les gaulois
        MainBoot.getInstance().addAgent(Constants.asterix.getLocalName(), "personnagesSpeciaux.Gaulois.Asterix", new Object[]{location});

        location = getNewPosGaulois(); //position de d�ｾ�ｽｩpart random pour les gaulois
        MainBoot.getInstance().addAgent(Constants.obelix.getLocalName(), "personnagesSpeciaux.Gaulois.Obelix", new Object[]{location});

        location = Constants.POSPANORAMIX; //position de d�ｾ�ｽｩpart random pour les gaulois
        MainBoot.getInstance().addAgent(Constants.panoramix.getLocalName(), "personnagesSpeciaux.Gaulois.Panoramix", new Object[]{location});

    }

    private void addRomainsSpeciaux() {
        Int2D location = Constants.POSCESAR; //position de d�ｾ�ｽｩpart random pour les gaulois
        MainBoot.getInstance().addAgent(Constants.cesar.getLocalName(), "personnagesSpeciaux.Romain.Cesar", new Object[]{location});
    }

    private void addZoneAbraCells() {
        int Xmin = Math.max(Constants.POSPANORAMIX.x - Constants.DELTAPANORAMIX, 0);
        int Xmax = Math.min(Constants.POSPANORAMIX.x + Constants.DELTAPANORAMIX, Constants.GRID_SIZE);
        int Ymin = Math.max(Constants.POSPANORAMIX.y - Constants.DELTAPANORAMIX, 0);
        int Ymax = Math.min(Constants.POSPANORAMIX.y + Constants.DELTAPANORAMIX, Constants.GRID_SIZE);
        for (int i = Xmin; i < Xmax; i++) {
            for (int j = Ymin; j < Ymax; j++) {
                Int2D location = new Int2D(i, j);
                ZoneAbra e = new ZoneAbra();
                yard.setObjectLocation(e, location);
            }
        }
    }

    private void addZonePanoCells() {
        int Xmin = Math.max(Constants.POSABRARACOURCIX.x - Constants.DELTAABRARACOURCIX, 0);
        int Xmax = Math.min(Constants.POSABRARACOURCIX.x + Constants.DELTAABRARACOURCIX, Constants.GRID_SIZE);
        int Ymin = Math.max(Constants.POSABRARACOURCIX.y - Constants.DELTAABRARACOURCIX, 0);
        int Ymax = Math.min(Constants.POSABRARACOURCIX.y + Constants.DELTAABRARACOURCIX, Constants.GRID_SIZE);
        for (int i = Xmin; i < Xmax; i++) {
            for (int j = Ymin; j < Ymax; j++) {

                Int2D location = new Int2D(i, j);
                ZonePano e = new ZonePano();
                yard.setObjectLocation(e, location);
            }
        }
    }

    private void addBanquetCells() {
        int Xmin = Constants.MINPOSBANQUET.x;
        int Xmax = Constants.MAXPOSBANQUET.x;
        int Ymin = Constants.MINPOSBANQUET.y;
        int Ymax = Constants.MAXPOSBANQUET.y;
        for (int i = Xmin; i < Xmax; i++) {
            for (int j = Ymin; j < Ymax; j++) {
                Int2D location = new Int2D(i, j);
                Banquet e = new Banquet();
                yard.setObjectLocation(e, location);
            }
        }
    }

    private void addMenhir() {
        Int2D location = Constants.posMenhir;
        Menhir e = new Menhir();
        yard.setObjectLocation(e, location);
    }

    private void addZoneMenhirCells() {
        int Xmin = Math.max(Constants.posMenhir.x - Constants.deltaMenhir, 0);
        int Xmax = Math.min(Constants.posMenhir.x + Constants.deltaMenhir, Constants.GRID_SIZE);
        int Ymin = Math.max(Constants.posMenhir.y - Constants.deltaMenhir, 0);
        int Ymax = Math.min(Constants.posMenhir.y + Constants.deltaMenhir, Constants.GRID_SIZE);
        for (int i = Xmin; i <= Xmax; i++) {
            for (int j = Ymin; j <= Ymax; j++) {
                Int2D location = new Int2D(i, j);
                ZoneMenhir e = new ZoneMenhir();
                yard.setObjectLocation(e, location);
            }
        }
    }


    //nb = borneInf+random.nextInt(borneSup-borneInf);

    public Int2D getNewPosGaulois() { //Entre les 2/3 et la droite
        return Maps.randomPosBetween(new Int2D(Constants.CAMPG_XMAX, Constants.CAMPG_YMAX), new Int2D(Constants.CAMPG_XMIN, Constants.CAMPG_YMIN));
    }
    
    public Int2D getNewPosAbra() { //Entre les 2/3 et la droite
        return Maps.randomPosBetween(new Int2D(Constants.POSABRARACOURCIX.x + Constants.DELTAABRARACOURCIX, 
        								Constants.POSABRARACOURCIX.y + Constants.DELTAABRARACOURCIX),
        							new Int2D(Constants.POSABRARACOURCIX.x - Constants.DELTAABRARACOURCIX,
        								Constants.POSABRARACOURCIX.y - Constants.DELTAABRARACOURCIX));
    }
    
    public Int2D getNewPosPano() { //Entre les 2/3 et la droite
        return Maps.randomPosBetween(new Int2D(Constants.POSPANORAMIX.x + Constants.DELTAPANORAMIX, 
										Constants.POSPANORAMIX.y + Constants.DELTAPANORAMIX),
									new Int2D(Constants.POSPANORAMIX.x - Constants.DELTAPANORAMIX,
										Constants.POSPANORAMIX.y - Constants.DELTAPANORAMIX));
    }
    
    public Int2D getNewPosMehnir() { //Entre les 2/3 et la droite
        return Maps.randomPosBetween(new Int2D(Constants.posMenhir.x + Constants.deltaMenhir, 
										Constants.posMenhir.y + Constants.deltaMenhir),
									new Int2D(Constants.posMenhir.x - Constants.deltaMenhir,
										Constants.posMenhir.y - Constants.deltaMenhir));
    }
    public Int2D getNewPosRomain() { //Entre la gauche et le tiers
        return Maps.randomPosBetween(new Int2D(Constants.CAMPR_XMAX, Constants.CAMPR_YMAX), new Int2D(Constants.CAMPR_XMIN, Constants.CAMPR_YMIN));
    }

    public Int2D getNewPosForet() { //Entre 1/3 et 2/3 sur x et entre 3/4 et le haut pour la foret
         return Maps.randomPosBetween(new Int2D(Constants.FORET_XMAX, Constants.FORET_YMAX), new Int2D(Constants.FORET_XMIN, Constants.FORET_YMIN));
    }

    public Int2D getNewPosBataille(int numeroRomain) {
		int x = Constants.CAMPR_XMAX + random.nextInt(Constants.CAMPG_XMIN - 2 * Constants.CAMPR_XMAX);
		int y = numeroRomain;
	    return new Int2D(x, y);
   }

}	