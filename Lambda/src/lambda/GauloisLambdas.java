package lambda;

import constants.Constants;
import main.MainBoot;
import sim.engine.SimState;
import sim.field.grid.SparseGrid2D;
import sim.util.Int2D;

public class GauloisLambdas extends SimState {

	private static final long serialVersionUID = 1L;
	private int nbGauloisL = Constants.N_GAULOIS_LAMBDA; //nb initial de gaulois lambdas
	public SparseGrid2D yard = new SparseGrid2D(Constants.GRID_SIZE, Constants.GRID_SIZE);
	
	public GauloisLambdas(long seed) {
		super(seed);
	}
	
	public void start() {
		super.start();
		yard.clear();
		addGauloisL();
	}
	
	private void addGauloisL() { 
		for(int  i  =  0;  i  <  Constants.N_GAULOIS_LAMBDA;  i++) {
			Int2D location = getNewPos(); //position de départ random pour les gaulois //A DELIMITER VILLAGE ET FORET????
			MainBoot.getInstance().addAgent("GauloisLambda"+i, "lambda.GauloisLambda", new Object[]{location});
		}
		nbGauloisL = Constants.N_GAULOIS_LAMBDA;
	}
	
	public Int2D getNewPos() {
		int x = this.random.nextInt(Constants.GRID_SIZE);
		int y = this.random.nextInt(Constants.GRID_SIZE);
		return new Int2D(x, y);
	}
	
}	
	
