package lambda;
import java.awt.Color;
import java.util.Random;

import constants.Constants;
import pathfinding.AStar;
import personnagesSpeciaux.Gaulois.Asterix;
import personnagesSpeciaux.Gaulois.Obelix;
import sim.engine.SimState;
import sim.portrayal.Portrayal;
import sim.portrayal.simple.OvalPortrayal2D;
import sim.util.Int2D;
import zones.champDeBataille;
public class RomainLambda extends Lambda {

	private static final long serialVersionUID = 1L;
	private Object[] equipementList; // Revoir le format
	protected boolean enBalade = false;
	protected int nPositionBalade = 0;
	@Override
	protected void setup() {
		super.setup();
		addBehaviour(new initZone(this,Constants.campRomain));
		addBehaviour(new attenteOrdreBataille(Constants.cesar));
		addBehaviour(new demandeDeSuppression(Constants.cesar));
		mettreUnCoup = false;
	}
	// ----------BEGIN -- GETTER & SETTER -- BEGIN----------//
	
	public boolean isEnBalade() {
		return enBalade;
	}

	public void setEnBalade(boolean enBalade) {
		this.enBalade = enBalade;
	}
	
	public Object[] getEquipementList() {
		return equipementList;
	}

	public void setEquipementList(Object[] equipementList) {
		this.equipementList = equipementList;
	}
	
	// ------------END -- GETTER & SETTER -- END------------//
	// ------------BEGIN -- ESPACE JADE -- BEGIN------------//
	// --------------END -- ESPACE JADE -- END--------------//
	
	@Override
	public void step(SimState state) {
		// TODO Cycle de vie
		if(isEnGuerre()) {
			if(this.getEtapeBataille() == Constants.DEBUT_GUERRE) {
				chemin = null;
				this.setEtapeBataille(Constants.INIT_POSITION_SUR_CHAMP);
			}
			else if(this.getEtapeBataille() == Constants.INIT_POSITION_SUR_CHAMP) {
				initPosition();
			}
			else if(this.getEtapeBataille() == Constants.ATTENDRE_GAULOIS) {
				if(this.pointDeVie < Constants.VIE_GAULOIS) {
					this.setEtapeBataille(Constants.SE_BATTRE);
				}
			}
			else if(this.getEtapeBataille() == Constants.SE_BATTRE) {
				combattre();
			}
		}
		else {
			seBalader();
		}
		updatePortrayal();
	}
	
	protected void initPosition() {
		if (chemin == null) { 
			int numeroRomain = Integer.parseInt(this.getLocalName().split("RomainLambda")[1]);
			positionToGo = Lambdas.getInstance().getNewPosBataille(numeroRomain);
			chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
		}
		if (position == positionToGo) {
			addBehaviour(new changementDeZone(this,Constants.campRomain,Constants.champDeBataille));
			champDeBataille.getInstance().getRomainsReady().add(this);
			this.setEtapeBataille(Constants.ATTENDRE_GAULOIS);
			chemin = null;
		} else {
			caseSuivante();
		}
	}
	
	protected void seBalader() {
		// TODO Aller chercher un sanglier en for�t
		if (chemin == null) { // On va vers le sanglier
			Random r = new Random();
			nPositionBalade = 1 + r.nextInt(Constants.MAX_BALADE);
			positionToGo = Lambdas.getInstance().getNewPosRomain();
			chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
		}
		
		if (position == positionToGo) { // On est sur le sanglier OU on est rentr�e au camp
			if(nPositionBalade > 0) {
				positionToGo = Lambdas.getInstance().getNewPosRomain();
				chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
				nPositionBalade--;
			}
			else {
				this.setEnBalade(false);
				chemin = null;
			}
		} else {
			caseSuivante();
		}
	}

	@Override
	protected Portrayal getObjectPortrayal() { //Representation d'un romain lambda
		OvalPortrayal2D r = new OvalPortrayal2D();
        if(getPointDeVie() > Constants.VIE_ROMAIN * 0.7) {
        	r.paint = Color.decode(("#000000"));
        } else if(getPointDeVie() > Constants.VIE_ROMAIN * 0.3) {
        	r.paint = Color.decode(("#999999"));
        } else if(getPointDeVie() > 0) {
        	r.paint = Color.decode(("#EEEEEE"));
        }
        else {
        	r.paint = Color.decode("#2798CC");
        }
        r.filled = true;
        return r;
    }
	
	protected void reculer() {
		position = new Int2D(this.position.x - 1, this.position.y);
		Lambdas.getInstance().getYard().setObjectLocation(this, position);
	}
	
	protected void avancer() {
		position = new Int2D(this.position.x + 1, this.position.y);
		Lambdas.getInstance().getYard().setObjectLocation(this, position);
	}
	
	public void mettreUnCoup(Lambda L) {
		L.prendreDegats(this.pointAttaque);
		mettreUnCoup = false;
		avancer();
	}
	
	protected void combattre() {
		if(this.mettreUnCoup) {
			GauloisLambda G = (GauloisLambda) voirEnnemiEnFace(new Int2D(this.getPosition().x + 1,this.getPosition().y),GauloisLambda.class);
			if(G != null) {
				this.mettreUnCoup(G);
			}
			else {
				Asterix A = (Asterix) voirEnnemiEnFace(new Int2D(this.getPosition().x + 1,this.getPosition().y),Asterix.class);
				if(A != null) {
					this.mettreUnCoup(A);
				}
				else {
					Obelix O = (Obelix) voirEnnemiEnFace(new Int2D(this.getPosition().x + 1,this.getPosition().y),Obelix.class);
					if(O != null) {
						this.mettreUnCoup(O);
					}
				}
			}
		}
	}
}
