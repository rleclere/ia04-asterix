package lambda;

import java.awt.Color;
import java.util.Random;

import com.fasterxml.jackson.databind.ObjectMapper;

import constants.Constants;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import pathfinding.AStar;
import personnagesSpeciaux.Gaulois.Abraracourcix;
import personnagesSpeciaux.Gaulois.Panoramix;
import ressources.Food;
import sim.engine.SimState;
import sim.portrayal.Portrayal;
import sim.portrayal.simple.OvalPortrayal2D;
import sim.util.Bag;
import sim.util.Int2D;
import zones.campGaulois;
import zones.Maps;
import zones.champDeBataille;

public class GauloisLambda extends Lambda {

	private static final long serialVersionUID = 1L;
	protected boolean boostPotion = false; // boost de la potion (compris entre 0 et Constants.TEMP_POTION)
	protected int tempsPotionRestant = Constants.TEMPS_POTION;
	protected boolean allerChercherSanglier = false;
	protected boolean allerDemanderTache = false;
	protected boolean enBalade = false;
	protected int nPositionBalade = 0;
	protected int fatigue = Constants.N_ACTION_PERTE_VIE;
	protected RomainLambda adversaire = null;

	@Override
	protected void setup() {
		super.setup();
		addBehaviour(new initZone(this, Constants.campGaulois)); // EXAMPLE, si Lambda a besoin d'�tre un agent JADE
		addBehaviour(new attenteOrdreBataille(Constants.abraracourcix));
		mettreUnCoup = true;
	}

	// ----------BEGIN -- GETTER & SETTER -- BEGIN----------//
	public boolean isBoostPotion() {
		return boostPotion;
	}

	public void setBoostPotion(boolean bootPotion) {
		this.boostPotion = bootPotion;
	}

	public boolean isAllerChercherSanglier() {
		return allerChercherSanglier;
	}

	public void setAllerChercherSanglier(boolean allerChercherSanglier) {
		this.allerChercherSanglier = allerChercherSanglier;
	}

	public boolean isAllerDemanderTache() {
		return allerDemanderTache;
	}

	public void setAllerDemanderTache(boolean allerDemanderTache) {
		this.allerDemanderTache = allerDemanderTache;
	}

	public boolean isEnBalade() {
		return enBalade;
	}

	public void setEnBalade(boolean enBalade) {
		this.enBalade = enBalade;
	}
	// ------------END -- GETTER & SETTER -- END------------//
	// ------------BEGIN -- ESPACE JADE -- BEGIN------------//

	protected class demandeFood extends Behaviour { // Utiliser pour que les zones puissent savoir qui se trouve

		private static final long serialVersionUID = 1L;
		private boolean test = false;

		public demandeFood() {
			ACLMessage demande = new ACLMessage(Constants.GLDemandeFood);
			demande.addReceiver(Constants.campGaulois);
			send(demande);
		}

		@Override
		public void action() {
			MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchPerformative(Constants.GLDemandeFood),
					MessageTemplate.MatchSender(Constants.campGaulois));
			ACLMessage reponse = receive(mt);
			if (reponse != null) {
				test = true;
				if (reponse.getContent().equals("1")) {
					System.out.println("Je mange");
					pointDeVie += Constants.SANGLIER;
				} else if (getPointDeVie() > 1) {
					setAllerChercherSanglier(true);
				}
				updatePortrayal();
			} else {
				block();
			}
		}

		@Override
		public boolean done() {
			return test;
		}
	}

	protected class demandeDeTache extends Behaviour { // Utiliser pour que les zones puissent savoir qui se
														// trouve o�
		private static final long serialVersionUID = 1L;
		GauloisLambda GL;

		public demandeDeTache(GauloisLambda GL) {
			this.GL = GL;
			ACLMessage demande = new ACLMessage(Constants.GLDemandeDeTache);
			demande.addReceiver(Constants.abraracourcix);
			chemin = null;
			send(demande);
		}

		@Override
		public void action() {
			MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchPerformative(Constants.GLDemandeDeTache),
					MessageTemplate.MatchSender(Constants.abraracourcix));
			ACLMessage reponse = receive(mt);
			if (reponse != null) {
				if (!reponse.getContent().equals("0")) {
					positionToGo = fromJSON(reponse.getContent());
					GL.setAllerChercherSanglier(true);
				} else {
					GL.setEnBalade(true);
				}
				GL.setAllerDemanderTache(false);

			} else {
				block();
			}
		}

		@Override
		public boolean done() {
			return !GL.isAllerDemanderTache();
		}
	}

	protected class boirePotion extends Behaviour { // Utiliser pour que les zones puissent savoir qui se trouve
													// o�
		private static final long serialVersionUID = 1L;
		private boolean test = false;

		public boirePotion() {
			ACLMessage demande = new ACLMessage(Constants.GLDemandePotion);
			demande.addReceiver(Constants.campGaulois);
			send(demande);
		}

		@Override
		public void action() {
			MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchPerformative(Constants.GLDemandePotion),
					MessageTemplate.MatchSender(Constants.campGaulois));
			ACLMessage reponse = receive(mt);
			if (reponse != null) {
				if (reponse.getContent().equals("1")) {
					setBoostPotion(true);
					test = true;
				}
			} else {
				block();
			}
		}

		@Override
		public boolean done() {
			// TODO Auto-generated method stub
			return test;
		}
	}

	// --------------END -- ESPACE JADE -- END--------------//

	@Override
	public void step(SimState state) {
		// TODO Cycle de vie
		if (isBoostPotion()) {
			if (this.tempsPotionRestant == 0) {
				this.setBoostPotion(false);
				this.tempsPotionRestant = Constants.TEMPS_POTION;
			} else {
				this.tempsPotionRestant--;
			}
		}
		if (isEnGuerre()) {
			// TODO en guerre
			guerre();
		} else {
			if (this.isAllerChercherSanglier()) {
				allerChercherSanglier();

			} else if (pointDeVie < Constants.MAX_VIE_LAMBDA - Constants.SANGLIER) {
				// TODO si on est dans le bonne zone
				addBehaviour(new demandeFood());
			} else if (this.isAllerDemanderTache()) {
				allerDemanderTache();

			} else if (this.isEnBalade()) {
				seBalader();
			} else {
				this.setAllerDemanderTache(true);
			}
		}
		// TODO Prioriser les t�ches : Guerre, Chercher Sanglier, Chercher Potion,
		// Manger
		updatePortrayal();
	}

	protected void guerre() {
		if (this.getEtapeBataille() == Constants.DEBUT_GUERRE) {
			// TODO On stop les actions de la vie classique
			initGuerre();
		}
		if (this.getEtapeBataille() == Constants.ALLER_BOIRE_POTION) {
			allerBoirePotion();
		}

		if (this.getEtapeBataille() == Constants.INIT_POSITION_SUR_CHAMP) {
			initPosition();
		}

		if (this.getEtapeBataille() == Constants.SE_BATTRE) {
			combattre();
		}

		if (this.getEtapeBataille() == Constants.FIN_GUERRE) {
			this.setEtapeBataille(Constants.BANQUET);
		}

		if (this.getEtapeBataille() == Constants.BANQUET) {
			feterLaVictoire();
		}
		if (this.getEtapeBataille() == Constants.ATTENDRE_GAULOIS) {
			if (campGaulois.getInstance().getNbGauloisLambda() == campGaulois.getInstance()
					.getNbGauloisLambdaRestant() && campGaulois.getInstance().isAsterixHere() && campGaulois.getInstance().isObelixHere()) {
				this.setEnGuerre(false);
				this.setEtapeBataille(Constants.DEBUT_GUERRE);
			}
		}
	}

	protected void combattre() {
		if (adversaire.getPointDeVie() > 0) {
			if (mettreUnCoup) {
				this.mettreUnCoup(adversaire);
			} else {
				if (voirEnnemiEnFace(new Int2D(this.getPosition().getX() - 1, this.getPosition().getY()),
						RomainLambda.class) != adversaire) {
					if (chemin == null) {
						positionToGo = new Int2D(adversaire.getPosition().x + 1, adversaire.getPosition().getY());
						chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
					}

				} 
				if (chemin != null) {
					if (position == positionToGo) {
						mettreUnCoup = true;
						chemin = null;
					} else {
						caseSuivante();
					}
				}
			}
		} else {
			adversaire = null;
			chemin = null;
			this.setEtapeBataille(Constants.BANQUET);

		}
	}

	protected void feterLaVictoire() {
		if (chemin == null) {
			int numeroGaulois;
			if(this.getLocalName().contentEquals(Constants.asterix.getLocalName())) {
				numeroGaulois = Constants.N_GAULOIS_LAMBDA;
			}
			else if(this.getLocalName().contentEquals(Constants.obelix.getLocalName())) {
				numeroGaulois = Constants.N_GAULOIS_LAMBDA+1;
			}
			else {
				numeroGaulois = Integer.parseInt(this.getLocalName().split("GauloisLambda")[1]);
			}
			int xToGo = Constants.MINPOSBANQUET.x
					+ numeroGaulois % (Constants.MAXPOSBANQUET.x - Constants.MINPOSBANQUET.x);
			int yToGo = Constants.MINPOSBANQUET.y
					+ numeroGaulois / (Constants.MAXPOSBANQUET.y - Constants.MINPOSBANQUET.y);
			positionToGo = new Int2D(xToGo, yToGo);
			chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
		}
		if (position == positionToGo) {
			addBehaviour(new changementDeZone(this, Constants.champDeBataille, Constants.campGaulois));
			this.setEtapeBataille(Constants.ATTENDRE_GAULOIS);
		} else {
			caseSuivante();
		}

	}

	protected void initPosition() {
		if (chemin == null) {
			// TODO MODIFICATION ICI A FAIRE
			int numeroGaulois = Integer.parseInt(this.getLocalName().split("GauloisLambda")[1]);
			addBehaviour(new changementDeZone(this, Constants.campGaulois, Constants.champDeBataille));
			adversaire = champDeBataille.getInstance().getRomainsReady().get(numeroGaulois);
			positionToGo = new Int2D(adversaire.getPosition().getX() + 1, adversaire.getPosition().getY());
			chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
		}
		if (position == positionToGo) {
			mettreUnCoup = true;
			chemin = null;
			this.setEtapeBataille(Constants.SE_BATTRE);
		} else {
			caseSuivante();
		}
	}

	protected void initGuerre() {
		// Aller chercher sanglier et la seule action qui pourrait poser problème si
		// elle devait reprendre ensuite
		if (this.isAllerChercherSanglier() && zonePosition(positionToGo).equals("foret")) {
			// Si on allait chercher un sanglier, on prévient Abraracourcix qu'on est pas
			// allé le chercher
			Food sanglier = new Food(positionToGo, "Sanglier", Constants.LocationType.Forest);
			Abraracourcix.getInstance().getSangliersDansForet().add(sanglier);
		}
		this.setAllerChercherSanglier(false);
		chemin = null;
		this.setEtapeBataille(Constants.ALLER_BOIRE_POTION);
	}

	protected void allerBoirePotion() {
		if (chemin == null) {
			positionToGo = Panoramix.getInstance().getPosition();
			chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
		}
		if (position == positionToGo) {
			demanderPotionPano();
			chemin = null;
			this.setEtapeBataille(Constants.INIT_POSITION_SUR_CHAMP);
		} else {
			caseSuivante();
		}
	}

	protected void demanderPotionPano() {
		addBehaviour(new boirePotion());
	}

	protected void fatigue() {
		this.fatigue--;
		if (fatigue == 0) {
			fatigue = Constants.N_ACTION_PERTE_VIE;
			this.pointDeVie--;
			updatePortrayal();
		}
	}

	protected void seBalader() {
		// TODO Aller chercher un sanglier en for�t
		if (chemin == null) { // On va vers le sanglier
			Random r = new Random();
			nPositionBalade = 1 + r.nextInt(Constants.MAX_BALADE);
			positionToGo = Lambdas.getInstance().getNewPosGaulois();
			chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
		}

		if (position == positionToGo) { // On est sur le sanglier OU on est rentr�e au camp
			if (nPositionBalade > 0) {
				positionToGo = Lambdas.getInstance().getNewPosGaulois();
				chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
				nPositionBalade--;
			} else {
				this.setEnBalade(false);
				chemin = null;
			}
			fatigue();
		} else {
			caseSuivante();
		}
	}

	protected void allerChercherSanglier() {
		// TODO Aller chercher un sanglier en for�t
		if (chemin == null) { // On va vers le sanglier
			chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
		}
		if (position == positionToGo) { // On est sur le sanglier OU on est rentr�e au camp

			if (zoneDePresence() == "campG") {
				donnerSanglier();
				updatePortrayal();
				chemin = null;
			} else if (zoneDePresence() == "foret") {
				// TODO prendre sanglier
				supprimerSanglier(position);
				this.positionToGo = Maps.randomPosBetween(Constants.MAXPOSBANQUET, Constants.MINPOSBANQUET);
				chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
				updatePortrayal();
			}
			fatigue();
		} else {
			caseSuivante();
		}
	}

	protected void allerDemanderTache() {
		if (chemin == null) { // On va vers le chef
			positionToGo = Abraracourcix.getInstance().getPosition();
			chemin = AStar.pathfinding(Lambdas.getInstance().getYard(), position, positionToGo);
		}
		if (position == positionToGo) { // on est � cot� du chef
			addBehaviour(new demandeDeTache(this));
			chemin = null;
			fatigue();
		} else {
			caseSuivante();
		}
	}

	public void attraperSanglier(Int2D positionSanglier) {
		// TODO

	}

	@Override
	public void mettreUnCoup(Lambda L) {
		if (boostPotion) {
			L.prendreDegats((int) (this.pointAttaque * Constants.BOOST_POTION));
		} else {
			L.prendreDegats(this.pointAttaque);
		}
		mettreUnCoup = false;
		avancer();
	}

	public void donnerSanglier() {
		ACLMessage GLprevientAbra = new ACLMessage(Constants.GLTacheTermine);
		ACLMessage GLDonneCamp = new ACLMessage(Constants.GLDonneSanglier);
		GLprevientAbra.addReceiver(Constants.abraracourcix);
		GLDonneCamp.addReceiver(Constants.campGaulois);
		this.setAllerChercherSanglier(false);
		GLDonneCamp.setContent(this.position.toCoordinates());
		
		// TODO : Ameliorier ce fix visuel
		if(Maps.isBetween(this.position, Constants.MAXPOSBANQUET, Constants.MINPOSBANQUET)) {
			Food sanglier = new Food(this.position, "Sanglier", null);
			Lambdas.getInstance().getYard().setObjectLocation(sanglier, this.position);
		}
		
		send(GLprevientAbra);
		send(GLDonneCamp);
	}

	public Int2D fromJSON(String chaineList) {
		ObjectMapper mapper = new ObjectMapper();
		Int2D s = new Int2D();
		try {
			// TODO a tester
			s = mapper.readValue(chaineList, Int2D.class);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return s;
	}

	public synchronized void supprimerSanglier(Int2D position) {
		Bag b = Lambdas.getInstance().getYard().getObjectsAtLocation(position);
		if (b != null) {
			Object[] agentsOnCell = b.objs;
			for (Object agent : agentsOnCell) {
				if (agent != null && agent.getClass() == Food.class) {
					Food food = (Food) agent;
					if (food.getResName().equals("Sanglier")) {
						try {
							Lambdas.getInstance().getYard().remove(agent);
							Lambdas.getInstance().addNewSanglier();
							return;
						}
						catch(Exception e) {
							System.out.println("erreur");
						}
					}
				}
			}
		}

	}

	protected Portrayal getObjectPortrayal() { // Representation d'un gaulois lambda
		OvalPortrayal2D r = new OvalPortrayal2D();
		if (this.isBoostPotion()) {
			r.paint = Color.decode(("#00FF00"));
		} else {
			if (getPointDeVie() > Constants.VIE_GAULOIS * 0.7) {
				r.paint = Color.decode(("#FFD700"));
			} else if (getPointDeVie() > Constants.VIE_GAULOIS * 0.3) {
				r.paint = Color.decode(("#FF8C00"));
			} else {
				r.paint = Color.decode(("#FF0000"));
			}
		}
		r.filled = true;
		return r;
	}

	protected void reculer() {
		position = new Int2D(this.position.x + 1, this.position.y);
		Lambdas.getInstance().getYard().setObjectLocation(this, position);
	}

	protected void avancer() {
		position = new Int2D(this.position.x - 1, this.position.y);
		Lambdas.getInstance().getYard().setObjectLocation(this, position);
	}

}
