package lambda;

import java.util.ArrayList;

import constants.Constants;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import pathfinding.AStar;
import sim.engine.Steppable;
import sim.engine.Stoppable;
import sim.portrayal.Portrayal;
import sim.portrayal.grid.SparseGridPortrayal2D;
import sim.util.Bag;
import sim.util.Int2D;
import zones.campGaulois;
import zones.champDeBataille;

public abstract class Lambda extends Agent implements Steppable {

	private static final long serialVersionUID = 1L;

	protected int pointDeVie = Constants.VIE;
	protected int pointDeForce = Constants.FORCE;
	protected int pointDeMoral = Constants.MORAL;
	protected int pointAttaque = Constants.ATTAQUE;
	protected int pointDeplacement = Constants.DISTANCE_DEPLACEMENT;
	protected int tailleInventaire = Constants.INVENTAIRE;
	protected int nObjetsDansInventaire = 0;
	protected Object[] inventaire = new Object[tailleInventaire];
	
	protected Int2D position;
	protected Int2D positionToGo;
	protected ArrayList<Int2D> chemin = null;
	protected int etapeBataille = Constants.DEBUT_GUERRE;
	protected boolean mettreUnCoup = false;
	
	protected Stoppable stoppable;

	protected boolean enGuerre = false;

	@Override
	protected void setup() {
		this.position = (Int2D) this.getArguments()[0];
		Lambdas.getInstance().getYard().setObjectLocation(this, position);
		stoppable = Lambdas.getInstance().schedule.scheduleRepeating(this);
		this.setStoppable(stoppable);
		updatePortrayal();
	}
	
	// ----------BEGIN -- GETTER & SETTER -- BEGIN----------//
	
	public int getEtapeBataille() {
		return etapeBataille;
	}

	public void setEtapeBataille(int etapeBataille) {
		this.etapeBataille = etapeBataille;
	}
	
	public boolean isEnGuerre() {
		return enGuerre;
	}

	public void setEnGuerre(boolean enGuerre) {
		this.enGuerre = enGuerre;
	}
	
	public int getPointDeVie() {
		return pointDeVie;
	}

	public void setPointDeVie(int pointDeVie) {
		this.pointDeVie = pointDeVie;
	}

	public int getPointDeForce() {
		return pointDeForce;
	}

	public void setPointDeForce(int pointDeForce) {
		this.pointDeForce = pointDeForce;
	}

	public int getPointDeMoral() {
		return pointDeMoral;
	}

	public void setPointDeMoral(int pointDeMoral) {
		this.pointDeMoral = pointDeMoral;
	}

	public int getPointAttaque() {
		return pointAttaque;
	}

	public void setPointAttaque(int pointAttaque) {
		this.pointAttaque = pointAttaque;
	}

	public int getPointDeplacement() {
		return pointDeplacement;
	}

	public void setPointDeplacement(int pointDeplacement) {
		this.pointDeplacement = pointDeplacement;
	}

	public Int2D getPosition() {
		return position;
	}

	public void setPosition(Int2D position) {
		this.position = position;
	}
	
	public void setPosition(int x, int y) {
		this.position = new Int2D(x,y);
	}
	
	public Object[] getInventaire() {
		return inventaire;
	}

	public void setInventaire(Object[] inventaire) {
		this.inventaire = inventaire;
	}
	
	public int getnObjetsDansInventaire() {
		return nObjetsDansInventaire;
	}

	public void setnObjetsDansInventaire(int nObjetsDansInventaire) {
		this.nObjetsDansInventaire = nObjetsDansInventaire;
	}

	public Stoppable getStoppable() {
		return stoppable;
	}

	public void setStoppable(Stoppable stoppable) {
		this.stoppable = stoppable;
	}

	// ------------END -- GETTER & SETTER -- END------------//
	// ------------BEGIN -- ESPACE JADE -- BEGIN------------//

	protected class demandeDeSuppression extends Behaviour {

		AID chef;
		boolean test = false;
		public demandeDeSuppression(AID chef) {
			this.chef = chef;
		}
		private static final long serialVersionUID = 1L;
		@Override
		public void action() {
			MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchSender(chef),MessageTemplate.MatchPerformative(Constants.demandeDeSuppression));
			ACLMessage recu = receive(mt);
			if (recu != null) {
				Lambdas.getInstance().getYard().setObjectLocation(this.myAgent, new Int2D(0,0));
				setPointDeVie(0);
				updatePortrayal();
				this.myAgent.doDelete();
				test = true;
			} else {
				block();
			}
		}
		@Override
		public boolean done() {
			// TODO Auto-generated method stub
			return test;
		}
		
	}
	
	protected class attenteOrdreBataille extends CyclicBehaviour {

		AID chef;
		public attenteOrdreBataille(AID chef) {
			this.chef = chef;
		}
		private static final long serialVersionUID = 1L;
		@Override
		public void action() {
			MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchSender(chef),MessageTemplate.MatchPerformative(Constants.OrdreBataille));
			ACLMessage recu = receive(mt);
			if (recu != null && isEnGuerre() == false) {
				setEnGuerre(true);
				chemin = null;
			} else {
				block();
			}
		}
		
	}
	
	protected class changementDeZone extends OneShotBehaviour { // Utiliser pour que les zones puissent savoir qui se trouve o�ｿｽ
		private static final long serialVersionUID = 1L;
		private AID zoneDepart;
		private AID zoneArrivee;
		public changementDeZone(Lambda L,AID zoneDepart,AID zoneArrivee) {
			super(L);
			this.zoneDepart = zoneDepart;
			this.zoneArrivee = zoneArrivee;
		}

		public void action() {
			desinscription(zoneDepart);
			inscription(zoneArrivee);
		}
		
	}
	
	protected class initZone extends OneShotBehaviour { // Utiliser �ｿｽ la cr鬣荊ion d'un personnage pour l'ajouter dans un camps
		private static final long serialVersionUID = 1L;
		private AID zoneDepart;
		public initZone(Lambda L,AID zoneDepart) {
			super(L);
			this.zoneDepart = zoneDepart;
		}

		public void action() {
			inscription(zoneDepart);
		}
		
	}
	
	private void inscription(AID zone) {
		ACLMessage message = new ACLMessage(ACLMessage.SUBSCRIBE); // SUBSCRIBE = inscription
		message.addReceiver(zone);
		send(message);
	}
	
	private void desinscription(AID zone) {
		ACLMessage message = new ACLMessage(ACLMessage.CANCEL); // CANCEL = descinscription
		message.addReceiver(zone);
		send(message);
	}

	// ------------END -- ESPACE JADE -- END------------//
	
	public void prendreDegats(int degats) {
		this.setPointDeVie(Math.max(0, this.getPointDeVie() - degats));
		if (this.getPointDeVie() <= 0) {
			this.updatePortrayal();
			Lambdas.getInstance().getYard().setObjectLocation(this, new Int2D(0,0));
			this.stoppable.stop();
			if(this.getLocalName().contains("GauloisLambda")) {
				System.out.println("Je suis Gaulois");
				campGaulois.getInstance().setNbGauloisLambdaRestant(campGaulois.getInstance().getNbGauloisLambdaRestant() - 1);
			}
			if(this.getLocalName().contains("RomainLambda")) {
				champDeBataille.getInstance().desinscriptionAgent(this.getAID());
			}
		}
		else {
			mettreUnCoup = true;
			reculer();
		}
	}

	protected abstract void reculer();
	protected abstract void avancer();
	
	public void Attaquer(Lambda typePerso) {
		// TODO d鬥ｭinir une animation de combat ?
		typePerso.setPointDeVie(typePerso.getPointDeVie() - this.getPointAttaque());
		
	}
	
	public void perteMoral(int perteMoral) {
		this.setPointDeMoral(this.getPointDeMoral() - perteMoral);
		// TODO Cons鬧ｲuence sur les points de force ?
	}
	
	public void Recolter(Object objetRecolte) {
		// TODO d鬧ｱlace l'objet de la case �ｿｽ un attribut du lambda
		this.inventaire[this.nObjetsDansInventaire] = objetRecolte;
		this.nObjetsDansInventaire++;
	}
	
	public void manger(int PV) {
		// TODO Lambda nourriture/ manger diff pour Romain et Gaulois ?
		this.setPointDeVie(this.getPointDeVie() + PV);
	}
	
	public void dormir() {
		// TODO augmente les points de vies, Dormir remonte aussi le moral ?
		this.setPointDeVie(this.getPointDeVie() + Constants.DORMIR);
	}
	
	public abstract void mettreUnCoup(Lambda L);
	
	public Lambda voirEnnemiEnFace(Int2D positionEnnemi, Object classEnnemi) {
		Bag b = Lambdas.getInstance().getYard().getObjectsAtLocation(positionEnnemi);
		if(b != null) {
			Object[] objectOnCell = b.objs;
			for(Object Agent : objectOnCell) {
				if(Agent != null && Agent.getClass() == classEnnemi) {
					return (Lambda)Agent;
				}
			}
		}
		return null;
	}
	
	public String zoneDePresence() {
		int x = position.getX();
		if(x > Constants.CAMPG_XMIN) { // Camp gaulois
			return "campG";
		}
		else if(x < Constants.CAMPR_XMAX) { // Camp romain
			return "campR";
		}
		else { // Bataille ou foret
			int y = position.getY();
			if(y < Constants.FORET_YMAX) { // For鮟�
				return "foret";
			}
			else { // Bataille
				return "bataille";
			}
		}
	}
	
	public String zonePosition(Int2D positionToGo) {
		int x = positionToGo.getX();
		if(x > Constants.CAMPG_XMIN) { // Camp gaulois
			return "campG";
		}
		else if(x < Constants.CAMPR_XMAX) { // Camp romain
			return "campR";
		}
		else { // Bataille ou foret
			int y = position.getY();
			if(y < Constants.FORET_YMAX) { // For鮟�
				return "foret";
			}
			else { // Bataille
				return "bataille";
			}
		}
	}
	
	public void caseSuivante() {
		// D駱lacement du personnage si la case est libre
		if (chemin != null && chemin.size() > 1) {
			if (AStar.cellIsEmpty(Lambdas.getInstance().getYard(), chemin.get(1))) {
				try {
					position = chemin.get(1);
					chemin.remove(0);
					Lambdas.getInstance().getYard().setObjectLocation(this, position);
				}
				catch(NullPointerException nPE) {
					System.out.println("Erreur");
				}
			}
		} else {
			position = positionToGo;
			Lambdas.getInstance().getYard().setObjectLocation(this, position);
		}
	}
	
	protected void updatePortrayal() {
		SparseGridPortrayal2D yardPortrayal = Lambdas.getInstance().getYardPortrayal();
	    yardPortrayal.setPortrayalForObject(this, getObjectPortrayal());
	}
	
	protected abstract Portrayal getObjectPortrayal();
}
