package main;
import constants.Constants;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;
import lambda.Lambdas;
import sim.display.Console;
import sim.portrayal.grid.SparseGridPortrayal2D;
import view.View;

public class MainBoot {
	public static String MAIN_PROPERTIES_FILE = "./src/main/main_container.txt"; // TODO Modifier le lien vers le fichier de config
	static AgentContainer mc = null;
	private static MainBoot INSTANCE = null;
	
	public static void main(String[] args) {
		jadeBootGUI();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//appel mason boot
		masonBoot();
	}
	
	public static void jadeBootGUI() {
		MainBoot mb = MainBoot.getInstance();
	}
	
	public static MainBoot getInstance()
    {           
		// TODO IMPORTANT Methode à appeler dès le lancement de l'application
        if (INSTANCE == null){   
        	INSTANCE = new MainBoot();
        }
        return INSTANCE;
    }
	
	private MainBoot() { // Initialisation Jade
		Runtime rt = Runtime.instance();
		Profile p = null;
		try {
			p = new ProfileImpl(MAIN_PROPERTIES_FILE);
			mc = rt.createMainContainer(p);
			AgentController agentCampGaulois = mc.createNewAgent(Constants.campGaulois.getLocalName(), "zones.campGaulois", null);
			AgentController agentCampRomain = mc.createNewAgent(Constants.campRomain.getLocalName(), "zones.campRomain", null);
			AgentController agentForet = mc.createNewAgent(Constants.foret.getLocalName(), "zones.foret", null);
			AgentController agentChampDeBataille = mc.createNewAgent(Constants.champDeBataille.getLocalName(), "zones.champDeBataille", null);
			agentCampGaulois.start();
			agentCampRomain.start();
			agentForet.start();
			agentChampDeBataille.start();
		} catch (Exception ex) {
			ex.printStackTrace();
			System.exit(-1);
		}
	}

	public void addAgent(String nom, String classe, Object[] lists) {
		AgentController A;
		try {
			A = mc.createNewAgent(nom,classe, lists);
			A.start();
		} catch (StaleProxyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void masonBoot() {
		Lambdas model = Lambdas.getInstance();
		View gui = new View(model, model.getYardPortrayal());
		Console console = new Console(gui);
		console.setVisible(true);
	}
}
